package org.bender.enums;

/**
 * @author CodeBender-zhj
 * @date 2020/8/10
 **/
public class RedisKeys {
    public static String IM_USER_USERID = "IM_USER_USERID";//用户登录的用户基本信息key

    public static String IM_USER_FRIEND = "IM_USER_FRIEND";//用户好友群组
}
