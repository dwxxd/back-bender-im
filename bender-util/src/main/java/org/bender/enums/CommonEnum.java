package org.bender.enums;

/**
 * @author CodeBender-zhj
 * @date 2020/7/28
 **/
public enum CommonEnum {
    DELETE(0,"删除/"),
    NOT_DELETE(1,"未删除/正常");


    private Integer code;
    private String desc;

    CommonEnum() {
    }

    CommonEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
