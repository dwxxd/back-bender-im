package org.bender.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.Serializable;

/**
 * @author CodeBender-zhj
 * @date 2020/8/11
 **/
@Component
@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "登录验证失败或超时")
public class LoginException extends Exception  implements Serializable {

    private static final long serialVersionUID = 5759027883028274330L;


}

