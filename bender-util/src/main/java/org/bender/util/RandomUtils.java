package org.bender.util;

import java.util.Random;

/**
 * @author CodeBender-zhj
 * @date 2020/7/28
 **/
public class RandomUtils {

    public static void main(String[] args) {
        for (int i = 0; i < 8; i++) {
            System.err.println(randomNum(8));
        }
    }



    public static String randomNum(int count) {
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        //获取长度为6的字符串
        for (int i = 0; i < count; i++) {
            //获取范围在3之内的索引值
            int number = random.nextInt(3);
            int result = 0;
            switch (number) {
                case 0:
                    //Math.random()*25+65成成65-90的int型的整型,强转小数只取整数部分
                    result = (int) (Math.random() * 25 + 65);  //对应A-Z 参考ASCII编码表
                    //将整型强转为char类型
                    sb.append((char) result);
                    break;
                case 1:
                    result = (int) (Math.random() * 25 + 97);   //对应a-z
                    sb.append((char) result);
                    break;
                case 2:
                    sb.append(String.valueOf(new Random().nextInt(10)));
                    break;
            }
        }
        return sb.toString();
    }
}