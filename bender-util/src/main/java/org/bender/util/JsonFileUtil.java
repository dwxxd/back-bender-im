package org.bender.util;

import lombok.extern.log4j.Log4j2;


import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

/**
 * @author zenghj
 * @Date 2020/4/2
 */

@Log4j2
public class JsonFileUtil {

    public static String readJsonFile(String url) {
        String jsonStr = null;
        Reader reader = null;
        GZIPInputStream fileStream = null;
        try {
            URL fileUrl = new URL(url);
            HttpURLConnection urlCon = (HttpURLConnection) fileUrl.openConnection();
            urlCon.setConnectTimeout(6000);
            urlCon.setReadTimeout(6000);
            int code = urlCon.getResponseCode();
            if (code != HttpURLConnection.HTTP_OK) {
                throw new Exception("文件读取失败");
            }
            fileStream = new GZIPInputStream(urlCon.getInputStream());
            reader = new InputStreamReader(fileStream, "utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            jsonStr = sb.toString();
            jsonStr = jsonStr.replaceAll("\\n", ",");
            jsonStr = jsonStr.substring(0, jsonStr.length() - 1);
            jsonStr = "[" + jsonStr + "]";
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                reader.close();
            } catch (Exception e) {
            }
            try {
                fileStream.close();
            } catch (Exception e) {
            }
        }
        return jsonStr;
    }
}
