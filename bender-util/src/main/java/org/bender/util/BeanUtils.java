package org.bender.util;

import com.github.pagehelper.Page;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.context.ConfigurableApplicationContext;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;


public class BeanUtils extends org.apache.commons.beanutils.BeanUtils {

    /**
     * List转换，常用于DTO与Entity之间的转换等
     *
     * @param toClass    要转换成哪种class的list
     * @param targetList 目标List
     * @param <T>        要转换成的class类型
     * @return 返回新的List
     * @throws IllegalAccessException    err
     * @throws InvocationTargetException err
     * @throws InstantiationException    err
     */
    public static <T> List<T> copyListProperties(Class<T> toClass, List<?> targetList) {

        ArrayList<T> list = new Page<T>();
        try {
            if (targetList instanceof Page) {

                org.apache.commons.beanutils.BeanUtils.copyProperties(list, targetList);
            } else {
                list = new ArrayList<T>();
            }

            if (!CollectionUtils.isEmpty(targetList)) {
                for (Object obj : targetList) {
                    T t = toClass.newInstance();
                    org.apache.commons.beanutils.BeanUtils.copyProperties(t, obj);
                    list.add(t);
                }
            }
        } catch (Exception e) {

        }

        return list;
    }

    //将管理上下文的applicationContext设置成静态变量，供全局调用
    public static ConfigurableApplicationContext applicationContext;

    //定义一个获取已经实例化bean的方法
    public static <T> T getBean(Class<T> c) {
        return applicationContext.getBean(c);
    }


}
