package org.bender.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PageParam implements Serializable {

    private static final long serialVersionUID = 5425348267210823778L;
    private Integer pageNum = 1;
    private Integer pageSize = 20;
    private String searchKey;


}
