package org.bender.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zenghj
 * @Date 2019/11/29
 */

@Data
public class RequestParam<T> implements Serializable {

    private static final long serialVersionUID = -219602951996628993L;
    private Integer pageNum;

    private Integer pageSize;

    private T parm;

    public RequestParam() {
        pageNum =1 ;
        pageSize =10;
    }
}
