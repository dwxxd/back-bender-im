package org.bender.common;



import com.github.pagehelper.PageInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wei on 2016/12/12.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 2172667778548083036L;
    /**
     * 返回码
     */
    private int status;

    /**
     * 返回信息描述
     */
    private String message;

    /**
     * 返回数据
     */
    private T data;



    public static Result success(){
       return new Result(ResultStatus.SUCCESS.getCode(),ResultStatus.SUCCESS.getMsg(),null);
    }

    public static Result success(Object o){
       return new Result(ResultStatus.SUCCESS.getCode(),ResultStatus.SUCCESS.getMsg(),o);
    }


    public static Result success(List<Object> o){
       return new Result(ResultStatus.SUCCESS.getCode(),ResultStatus.SUCCESS.getMsg(),o);
    }


    public static Result success(PageInfo<Object> o){
       return new Result(ResultStatus.SUCCESS.getCode(),ResultStatus.SUCCESS.getMsg(),o);
    }

    public static Result fail(){
       return new Result(ResultStatus.NOT_SUCCESS.getCode(),ResultStatus.NOT_SUCCESS.getMsg(),null);
    }

    public static Result fail(Object o){
       return new Result(ResultStatus.NOT_SUCCESS.getCode(),ResultStatus.NOT_SUCCESS.getMsg(),o);
    }


    public static Result fail(List<Object> o){
       return new Result(ResultStatus.NOT_SUCCESS.getCode(),ResultStatus.NOT_SUCCESS.getMsg(),o);
    }


    public static Result fail(PageInfo<Object> o){
       return new Result(ResultStatus.NOT_SUCCESS.getCode(),ResultStatus.NOT_SUCCESS.getMsg(),o);
    }


    public static Result resultStatus(ResultStatus resultStatus){
        return new Result(resultStatus.getCode(),resultStatus.getMsg(),null);
    }


    public static Result resultStatus(ResultStatus resultStatus, Object o){
       return new Result(resultStatus.getCode(),resultStatus.getMsg(),o);
    }


    public static Result resultStatus(ResultStatus resultStatus, List<Object> o){
       return new Result(resultStatus.getCode(),resultStatus.getMsg(),o);
    }

    public static Result resultStatus(ResultStatus resultStatus, PageInfo<Object> o){
       return new Result(resultStatus.getCode(),resultStatus.getMsg(),o);
    }

}

