package org.bender;


import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;


@EnableDubbo
//@EnableCaching
@ComponentScan(basePackages={"org.bender"})
@MapperScan(basePackages = "org.bender.user.dao.mapper")
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class BenderUserRun {

	public static void main(String[] args)  {
		SpringApplication.run(BenderUserRun.class, args);

	}


}

