/* */
package org.bender.user.dao.mapper;

import org.bender.user.dao.entity.UserInfo;
import org.bender.user.dao.entity.UserInfoCriteria;
import org.springframework.stereotype.Repository;

/**
 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
@Repository
public interface UserInfoDAO extends IMapper<UserInfo, UserInfoCriteria, Long> {
}