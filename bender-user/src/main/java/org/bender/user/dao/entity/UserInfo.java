/* */
package org.bender.user.dao.entity;

import java.io.Serializable;
import java.util.Date;

/** 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
public class UserInfo implements Serializable {
    /** 
     * 串行版本ID
    */
    private static final long serialVersionUID = 7160990847667166016L;

    /** 
     * 用户id
     */ 
    private Long id;

    /** 
     * 昵称
     */ 
    private String nickName;

    /** 
     * 手机
     */ 
    private String phone;

    /** 
     */ 
    private String tagIds;

    /** 
     */ 
    private String tagCns;

    /** 
     */ 
    private Long counter;

    /** 
     */ 
    private Long city;

    /** 
     */ 
    private Long region;

    /** 
     */ 
    private String locationCn;

    /** 
     * 用户状态 0正常 1 被禁用  默认：0
     */ 
    private Integer status;

    /** 
     * 创建时间
     */ 
    private Date createTime;

    /** 
     * 背景图照片
     */ 
    private Long backImgFileId;

    /** 
     * 头像的文件id
     */ 
    private Long headImgFileId;

    /** 
     */ 
    private String remark;

    /** 
     * 获取 用户id user_info.id
     * @return 用户id
     */
    public final Long getId() {
        return id;
    }

    /** 
     * 设置 用户id user_info.id
     * @param id 用户id
     */
    public final void setId(Long id) {
        this.id = id;
    }

    /** 
     * 获取 昵称 user_info.nick_name
     * @return 昵称
     */
    public final String getNickName() {
        return nickName;
    }

    /** 
     * 设置 昵称 user_info.nick_name
     * @param nickName 昵称
     */
    public final void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    /** 
     * 获取 手机 user_info.phone
     * @return 手机
     */
    public final String getPhone() {
        return phone;
    }

    /** 
     * 设置 手机 user_info.phone
     * @param phone 手机
     */
    public final void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /** 
     * 获取 user_info.tag_ids
     * @return user_info.tag_ids
     */
    public final String getTagIds() {
        return tagIds;
    }

    /** 
     * 设置 user_info.tag_ids
     * @param tagIds user_info.tag_ids
     */
    public final void setTagIds(String tagIds) {
        this.tagIds = tagIds == null ? null : tagIds.trim();
    }

    /** 
     * 获取 user_info.tag_cns
     * @return user_info.tag_cns
     */
    public final String getTagCns() {
        return tagCns;
    }

    /** 
     * 设置 user_info.tag_cns
     * @param tagCns user_info.tag_cns
     */
    public final void setTagCns(String tagCns) {
        this.tagCns = tagCns == null ? null : tagCns.trim();
    }

    /** 
     * 获取 user_info.counter
     * @return user_info.counter
     */
    public final Long getCounter() {
        return counter;
    }

    /** 
     * 设置 user_info.counter
     * @param counter user_info.counter
     */
    public final void setCounter(Long counter) {
        this.counter = counter;
    }

    /** 
     * 获取 user_info.city
     * @return user_info.city
     */
    public final Long getCity() {
        return city;
    }

    /** 
     * 设置 user_info.city
     * @param city user_info.city
     */
    public final void setCity(Long city) {
        this.city = city;
    }

    /** 
     * 获取 user_info.region
     * @return user_info.region
     */
    public final Long getRegion() {
        return region;
    }

    /** 
     * 设置 user_info.region
     * @param region user_info.region
     */
    public final void setRegion(Long region) {
        this.region = region;
    }

    /** 
     * 获取 user_info.location_cn
     * @return user_info.location_cn
     */
    public final String getLocationCn() {
        return locationCn;
    }

    /** 
     * 设置 user_info.location_cn
     * @param locationCn user_info.location_cn
     */
    public final void setLocationCn(String locationCn) {
        this.locationCn = locationCn == null ? null : locationCn.trim();
    }

    /** 
     * 获取 用户状态 0正常 1 被禁用 user_info.status
     * @return 用户状态 0正常 1 被禁用
     */
    public final Integer getStatus() {
        return status;
    }

    /** 
     * 设置 用户状态 0正常 1 被禁用 user_info.status
     * @param status 用户状态 0正常 1 被禁用
     */
    public final void setStatus(Integer status) {
        this.status = status;
    }

    /** 
     * 获取 创建时间 user_info.create_time
     * @return 创建时间
     */
    public final Date getCreateTime() {
        return createTime;
    }

    /** 
     * 设置 创建时间 user_info.create_time
     * @param createTime 创建时间
     */
    public final void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /** 
     * 获取 背景图照片 user_info.back_img_file_id
     * @return 背景图照片
     */
    public final Long getBackImgFileId() {
        return backImgFileId;
    }

    /** 
     * 设置 背景图照片 user_info.back_img_file_id
     * @param backImgFileId 背景图照片
     */
    public final void setBackImgFileId(Long backImgFileId) {
        this.backImgFileId = backImgFileId;
    }

    /** 
     * 获取 头像的文件id user_info.head_img_file_id
     * @return 头像的文件id
     */
    public final Long getHeadImgFileId() {
        return headImgFileId;
    }

    /** 
     * 设置 头像的文件id user_info.head_img_file_id
     * @param headImgFileId 头像的文件id
     */
    public final void setHeadImgFileId(Long headImgFileId) {
        this.headImgFileId = headImgFileId;
    }

    /** 
     * 获取 user_info.remark
     * @return user_info.remark
     */
    public final String getRemark() {
        return remark;
    }

    /** 
     * 设置 user_info.remark
     * @param remark user_info.remark
     */
    public final void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append(", id=").append(id);
        sb.append(", nickName=").append(nickName);
        sb.append(", phone=").append(phone);
        sb.append(", tagIds=").append(tagIds);
        sb.append(", tagCns=").append(tagCns);
        sb.append(", counter=").append(counter);
        sb.append(", city=").append(city);
        sb.append(", region=").append(region);
        sb.append(", locationCn=").append(locationCn);
        sb.append(", status=").append(status);
        sb.append(", createTime=").append(createTime);
        sb.append(", backImgFileId=").append(backImgFileId);
        sb.append(", headImgFileId=").append(headImgFileId);
        sb.append(", remark=").append(remark);
        sb.append("]");
        return sb.toString();
    }
}