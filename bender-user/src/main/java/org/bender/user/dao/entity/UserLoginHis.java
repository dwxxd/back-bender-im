/* */
package org.bender.user.dao.entity;

import java.io.Serializable;
import java.util.Date;

/** 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
public class UserLoginHis implements Serializable {
    /** 
     * 串行版本ID
    */
    private static final long serialVersionUID = 6280473882403006648L;

    /** 
     * 绑定id
     */ 
    private Long id;

    /** 
     */ 
    private Long userId;

    /** 
     * 登录的 ip
     */ 
    private String ip;

    /** 
     * 登录的类型 
     */ 
    private Integer type;

    /** 
     * 创建时间
     */ 
    private Date createTime;

    /** 
     * 获取 绑定id user_login_his.id
     * @return 绑定id
     */
    public final Long getId() {
        return id;
    }

    /** 
     * 设置 绑定id user_login_his.id
     * @param id 绑定id
     */
    public final void setId(Long id) {
        this.id = id;
    }

    /** 
     * 获取 user_login_his.user_id
     * @return user_login_his.user_id
     */
    public final Long getUserId() {
        return userId;
    }

    /** 
     * 设置 user_login_his.user_id
     * @param userId user_login_his.user_id
     */
    public final void setUserId(Long userId) {
        this.userId = userId;
    }

    /** 
     * 获取 登录的 ip user_login_his.ip
     * @return 登录的 ip
     */
    public final String getIp() {
        return ip;
    }

    /** 
     * 设置 登录的 ip user_login_his.ip
     * @param ip 登录的 ip
     */
    public final void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    /** 
     * 获取 登录的类型  user_login_his.type
     * @return 登录的类型 
     */
    public final Integer getType() {
        return type;
    }

    /** 
     * 设置 登录的类型  user_login_his.type
     * @param type 登录的类型 
     */
    public final void setType(Integer type) {
        this.type = type;
    }

    /** 
     * 获取 创建时间 user_login_his.create_time
     * @return 创建时间
     */
    public final Date getCreateTime() {
        return createTime;
    }

    /** 
     * 设置 创建时间 user_login_his.create_time
     * @param createTime 创建时间
     */
    public final void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", ip=").append(ip);
        sb.append(", type=").append(type);
        sb.append(", createTime=").append(createTime);
        sb.append("]");
        return sb.toString();
    }
}