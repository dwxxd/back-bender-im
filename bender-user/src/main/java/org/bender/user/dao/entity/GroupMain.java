/* */
package org.bender.user.dao.entity;

import java.io.Serializable;
import java.util.Date;

/** 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
public class GroupMain implements Serializable {
    /** 
     * 串行版本ID
    */
    private static final long serialVersionUID = -8271601120852394926L;

    /** 
     * 群组id
     */ 
    private Long id;

    /** 
     * 群组名称
     */ 
    private String groupName;

    /** 
     */ 
    private Integer groupType;

    /** 
     * 创建人id
     */ 
    private Long createUserId;

    /** 
     * 群的token
     */ 
    private String groupToken;

    /** 
     * 所在国家
     */ 
    private Long country;

    /** 
     * 所在城市
     */ 
    private Long city;

    /** 
     * 所在地区
     */ 
    private Long region;

    /** 
     * 地区中文简称
     */ 
    private String locationCnFull;

    /** 
     * 创建时间
     */ 
    private Date createTime;

    /** 
     * 群状态 0 正常 1解散 2 被超级管理员禁用  默认：0
     */ 
    private Integer status;

    /** 
     */ 
    private String remark;

    /** 
     * 获取 群组id group_main.id
     * @return 群组id
     */
    public final Long getId() {
        return id;
    }

    /** 
     * 设置 群组id group_main.id
     * @param id 群组id
     */
    public final void setId(Long id) {
        this.id = id;
    }

    /** 
     * 获取 群组名称 group_main.group_name
     * @return 群组名称
     */
    public final String getGroupName() {
        return groupName;
    }

    /** 
     * 设置 群组名称 group_main.group_name
     * @param groupName 群组名称
     */
    public final void setGroupName(String groupName) {
        this.groupName = groupName == null ? null : groupName.trim();
    }

    /** 
     * 获取 group_main.group_type
     * @return group_main.group_type
     */
    public final Integer getGroupType() {
        return groupType;
    }

    /** 
     * 设置 group_main.group_type
     * @param groupType group_main.group_type
     */
    public final void setGroupType(Integer groupType) {
        this.groupType = groupType;
    }

    /** 
     * 获取 创建人id group_main.create_user_id
     * @return 创建人id
     */
    public final Long getCreateUserId() {
        return createUserId;
    }

    /** 
     * 设置 创建人id group_main.create_user_id
     * @param createUserId 创建人id
     */
    public final void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    /** 
     * 获取 群的token group_main.group_token
     * @return 群的token
     */
    public final String getGroupToken() {
        return groupToken;
    }

    /** 
     * 设置 群的token group_main.group_token
     * @param groupToken 群的token
     */
    public final void setGroupToken(String groupToken) {
        this.groupToken = groupToken == null ? null : groupToken.trim();
    }

    /** 
     * 获取 所在国家 group_main.country
     * @return 所在国家
     */
    public final Long getCountry() {
        return country;
    }

    /** 
     * 设置 所在国家 group_main.country
     * @param country 所在国家
     */
    public final void setCountry(Long country) {
        this.country = country;
    }

    /** 
     * 获取 所在城市 group_main.city
     * @return 所在城市
     */
    public final Long getCity() {
        return city;
    }

    /** 
     * 设置 所在城市 group_main.city
     * @param city 所在城市
     */
    public final void setCity(Long city) {
        this.city = city;
    }

    /** 
     * 获取 所在地区 group_main.region
     * @return 所在地区
     */
    public final Long getRegion() {
        return region;
    }

    /** 
     * 设置 所在地区 group_main.region
     * @param region 所在地区
     */
    public final void setRegion(Long region) {
        this.region = region;
    }

    /** 
     * 获取 地区中文简称 group_main.location_cn_full
     * @return 地区中文简称
     */
    public final String getLocationCnFull() {
        return locationCnFull;
    }

    /** 
     * 设置 地区中文简称 group_main.location_cn_full
     * @param locationCnFull 地区中文简称
     */
    public final void setLocationCnFull(String locationCnFull) {
        this.locationCnFull = locationCnFull == null ? null : locationCnFull.trim();
    }

    /** 
     * 获取 创建时间 group_main.create_time
     * @return 创建时间
     */
    public final Date getCreateTime() {
        return createTime;
    }

    /** 
     * 设置 创建时间 group_main.create_time
     * @param createTime 创建时间
     */
    public final void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /** 
     * 获取 群状态 0 正常 1解散 2 被超级管理员禁用 group_main.status
     * @return 群状态 0 正常 1解散 2 被超级管理员禁用
     */
    public final Integer getStatus() {
        return status;
    }

    /** 
     * 设置 群状态 0 正常 1解散 2 被超级管理员禁用 group_main.status
     * @param status 群状态 0 正常 1解散 2 被超级管理员禁用
     */
    public final void setStatus(Integer status) {
        this.status = status;
    }

    /** 
     * 获取 group_main.remark
     * @return group_main.remark
     */
    public final String getRemark() {
        return remark;
    }

    /** 
     * 设置 group_main.remark
     * @param remark group_main.remark
     */
    public final void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append(", id=").append(id);
        sb.append(", groupName=").append(groupName);
        sb.append(", groupType=").append(groupType);
        sb.append(", createUserId=").append(createUserId);
        sb.append(", groupToken=").append(groupToken);
        sb.append(", country=").append(country);
        sb.append(", city=").append(city);
        sb.append(", region=").append(region);
        sb.append(", locationCnFull=").append(locationCnFull);
        sb.append(", createTime=").append(createTime);
        sb.append(", status=").append(status);
        sb.append(", remark=").append(remark);
        sb.append("]");
        return sb.toString();
    }
}