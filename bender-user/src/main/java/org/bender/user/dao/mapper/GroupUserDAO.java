/* */
package org.bender.user.dao.mapper;

import org.bender.user.dao.entity.GroupUser;
import org.bender.user.dao.entity.GroupUserCriteria;
import org.springframework.stereotype.Repository;

/**
 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
@Repository
public interface GroupUserDAO extends IMapper<GroupUser, GroupUserCriteria, Long> {
}