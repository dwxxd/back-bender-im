/* */
package org.bender.user.dao.mapper;

import org.bender.user.dao.entity.UserBind;
import org.bender.user.dao.entity.UserBindCriteria;
import org.springframework.stereotype.Repository;

/**
 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
@Repository
public interface UserBindDAO extends IMapper<UserBind, UserBindCriteria, Long> {
}