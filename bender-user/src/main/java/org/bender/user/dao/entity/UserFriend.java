/* */
package org.bender.user.dao.entity;

import java.io.Serializable;
import java.util.Date;

/** 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
public class UserFriend implements Serializable {
    /** 
     * 串行版本ID
    */
    private static final long serialVersionUID = -599318383003765930L;

    /** 
     * 好友id
     */ 
    private Long id;

    /** 
     * 用户的id
     */ 
    private Long userId;

    /** 
     * 用户好友的id
     */ 
    private Long friendUserId;

    /** 
     * 好友昵称
     */ 
    private String nickName;

    /** 
     * 成为好友的时间
     */ 
    private Date createTime;

    /** 
     */ 
    private String remark;

    /** 
     * 获取 好友id user_friend.id
     * @return 好友id
     */
    public final Long getId() {
        return id;
    }

    /** 
     * 设置 好友id user_friend.id
     * @param id 好友id
     */
    public final void setId(Long id) {
        this.id = id;
    }

    /** 
     * 获取 用户的id user_friend.user_id
     * @return 用户的id
     */
    public final Long getUserId() {
        return userId;
    }

    /** 
     * 设置 用户的id user_friend.user_id
     * @param userId 用户的id
     */
    public final void setUserId(Long userId) {
        this.userId = userId;
    }

    /** 
     * 获取 用户好友的id user_friend.friend_user_id
     * @return 用户好友的id
     */
    public final Long getFriendUserId() {
        return friendUserId;
    }

    /** 
     * 设置 用户好友的id user_friend.friend_user_id
     * @param friendUserId 用户好友的id
     */
    public final void setFriendUserId(Long friendUserId) {
        this.friendUserId = friendUserId;
    }

    /** 
     * 获取 好友昵称 user_friend.nick_name
     * @return 好友昵称
     */
    public final String getNickName() {
        return nickName;
    }

    /** 
     * 设置 好友昵称 user_friend.nick_name
     * @param nickName 好友昵称
     */
    public final void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    /** 
     * 获取 成为好友的时间 user_friend.create_time
     * @return 成为好友的时间
     */
    public final Date getCreateTime() {
        return createTime;
    }

    /** 
     * 设置 成为好友的时间 user_friend.create_time
     * @param createTime 成为好友的时间
     */
    public final void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /** 
     * 获取 user_friend.remark
     * @return user_friend.remark
     */
    public final String getRemark() {
        return remark;
    }

    /** 
     * 设置 user_friend.remark
     * @param remark user_friend.remark
     */
    public final void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", friendUserId=").append(friendUserId);
        sb.append(", nickName=").append(nickName);
        sb.append(", createTime=").append(createTime);
        sb.append(", remark=").append(remark);
        sb.append("]");
        return sb.toString();
    }
}