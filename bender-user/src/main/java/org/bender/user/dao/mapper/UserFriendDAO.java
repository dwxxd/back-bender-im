/* */
package org.bender.user.dao.mapper;

import org.bender.user.dao.entity.UserFriend;
import org.bender.user.dao.entity.UserFriendCriteria;
import org.springframework.stereotype.Repository;

/**
 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
@Repository
public interface UserFriendDAO extends IMapper<UserFriend, UserFriendCriteria, Long> {
}