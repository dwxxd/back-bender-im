/* */
package org.bender.user.dao.entity;

import java.io.Serializable;
import java.util.Date;

/** 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
public class GroupUser implements Serializable {
    /** 
     * 串行版本ID
    */
    private static final long serialVersionUID = 3424142756971500889L;

    /** 
     * 群成员id
     */ 
    private Long id;

    /** 
     * 群组的id
     */ 
    private Long groupId;

    /** 
     * 用户的id
     */ 
    private Long userId;

    /** 
     * 成员在群里的昵称
     */ 
    private String nickName;

    /** 
     * 成员给群的备注昵称
     */ 
    private String groupNickName;

    /** 
     * 群成员类型 0 群主 1 管理员 2 普通成员
     */ 
    private Integer type;

    /** 
     * 是否被禁言 0 不是 1是  默认：0
     */ 
    private Integer unTalk;

    /** 
     * 消息免打扰 0 不是 1是  默认：0
     */ 
    private Integer unNotice;

    /** 
     * 加入的时间
     */ 
    private Date createTime;

    /** 
     * 获取 群成员id group_user.id
     * @return 群成员id
     */
    public final Long getId() {
        return id;
    }

    /** 
     * 设置 群成员id group_user.id
     * @param id 群成员id
     */
    public final void setId(Long id) {
        this.id = id;
    }

    /** 
     * 获取 群组的id group_user.group_id
     * @return 群组的id
     */
    public final Long getGroupId() {
        return groupId;
    }

    /** 
     * 设置 群组的id group_user.group_id
     * @param groupId 群组的id
     */
    public final void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    /** 
     * 获取 用户的id group_user.user_id
     * @return 用户的id
     */
    public final Long getUserId() {
        return userId;
    }

    /** 
     * 设置 用户的id group_user.user_id
     * @param userId 用户的id
     */
    public final void setUserId(Long userId) {
        this.userId = userId;
    }

    /** 
     * 获取 成员在群里的昵称 group_user.nick_name
     * @return 成员在群里的昵称
     */
    public final String getNickName() {
        return nickName;
    }

    /** 
     * 设置 成员在群里的昵称 group_user.nick_name
     * @param nickName 成员在群里的昵称
     */
    public final void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    /** 
     * 获取 成员给群的备注昵称 group_user.group_nick_name
     * @return 成员给群的备注昵称
     */
    public final String getGroupNickName() {
        return groupNickName;
    }

    /** 
     * 设置 成员给群的备注昵称 group_user.group_nick_name
     * @param groupNickName 成员给群的备注昵称
     */
    public final void setGroupNickName(String groupNickName) {
        this.groupNickName = groupNickName == null ? null : groupNickName.trim();
    }

    /** 
     * 获取 群成员类型 0 群主 1 管理员 2 普通成员 group_user.type
     * @return 群成员类型 0 群主 1 管理员 2 普通成员
     */
    public final Integer getType() {
        return type;
    }

    /** 
     * 设置 群成员类型 0 群主 1 管理员 2 普通成员 group_user.type
     * @param type 群成员类型 0 群主 1 管理员 2 普通成员
     */
    public final void setType(Integer type) {
        this.type = type;
    }

    /** 
     * 获取 是否被禁言 0 不是 1是 group_user.un_talk
     * @return 是否被禁言 0 不是 1是
     */
    public final Integer getUnTalk() {
        return unTalk;
    }

    /** 
     * 设置 是否被禁言 0 不是 1是 group_user.un_talk
     * @param unTalk 是否被禁言 0 不是 1是
     */
    public final void setUnTalk(Integer unTalk) {
        this.unTalk = unTalk;
    }

    /** 
     * 获取 消息免打扰 0 不是 1是 group_user.un_notice
     * @return 消息免打扰 0 不是 1是
     */
    public final Integer getUnNotice() {
        return unNotice;
    }

    /** 
     * 设置 消息免打扰 0 不是 1是 group_user.un_notice
     * @param unNotice 消息免打扰 0 不是 1是
     */
    public final void setUnNotice(Integer unNotice) {
        this.unNotice = unNotice;
    }

    /** 
     * 获取 加入的时间 group_user.create_time
     * @return 加入的时间
     */
    public final Date getCreateTime() {
        return createTime;
    }

    /** 
     * 设置 加入的时间 group_user.create_time
     * @param createTime 加入的时间
     */
    public final void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append(", id=").append(id);
        sb.append(", groupId=").append(groupId);
        sb.append(", userId=").append(userId);
        sb.append(", nickName=").append(nickName);
        sb.append(", groupNickName=").append(groupNickName);
        sb.append(", type=").append(type);
        sb.append(", unTalk=").append(unTalk);
        sb.append(", unNotice=").append(unNotice);
        sb.append(", createTime=").append(createTime);
        sb.append("]");
        return sb.toString();
    }
}