/* */
package org.bender.user.dao.entity;

import java.io.Serializable;
import java.util.Date;

/** 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
public class UserPassword implements Serializable {
    /** 
     * 串行版本ID
    */
    private static final long serialVersionUID = 3570049493865754288L;

    /** 
     */ 
    private Long id;

    /** 
     * 密码
     */ 
    private String password;

    /** 
     * 状态，0无效 1有效
     */ 
    private Integer status;

    /** 
     */ 
    private Long userId;

    /** 
     * 创建时间
     */ 
    private Date createTime;

    /** 
     * 获取 user_password.id
     * @return user_password.id
     */
    public final Long getId() {
        return id;
    }

    /** 
     * 设置 user_password.id
     * @param id user_password.id
     */
    public final void setId(Long id) {
        this.id = id;
    }

    /** 
     * 获取 密码 user_password.password
     * @return 密码
     */
    public final String getPassword() {
        return password;
    }

    /** 
     * 设置 密码 user_password.password
     * @param password 密码
     */
    public final void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /** 
     * 获取 状态，0无效 1有效 user_password.status
     * @return 状态，0无效 1有效
     */
    public final Integer getStatus() {
        return status;
    }

    /** 
     * 设置 状态，0无效 1有效 user_password.status
     * @param status 状态，0无效 1有效
     */
    public final void setStatus(Integer status) {
        this.status = status;
    }

    /** 
     * 获取 user_password.user_id
     * @return user_password.user_id
     */
    public final Long getUserId() {
        return userId;
    }

    /** 
     * 设置 user_password.user_id
     * @param userId user_password.user_id
     */
    public final void setUserId(Long userId) {
        this.userId = userId;
    }

    /** 
     * 获取 创建时间 user_password.create_time
     * @return 创建时间
     */
    public final Date getCreateTime() {
        return createTime;
    }

    /** 
     * 设置 创建时间 user_password.create_time
     * @param createTime 创建时间
     */
    public final void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append(", id=").append(id);
        sb.append(", password=").append(password);
        sb.append(", status=").append(status);
        sb.append(", userId=").append(userId);
        sb.append(", createTime=").append(createTime);
        sb.append("]");
        return sb.toString();
    }
}