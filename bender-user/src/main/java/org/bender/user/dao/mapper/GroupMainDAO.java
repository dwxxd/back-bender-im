/* */
package org.bender.user.dao.mapper;

import org.bender.user.dao.entity.GroupMain;
import org.bender.user.dao.entity.GroupMainCriteria;
import org.springframework.stereotype.Repository;

/**
 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
@Repository
public interface GroupMainDAO extends IMapper<GroupMain, GroupMainCriteria, Long> {
}