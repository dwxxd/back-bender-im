/* */
package org.bender.user.dao.mapper;

import org.bender.user.dao.entity.UserInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author CodeBender
 * date:2021/02/19 17:45
 */
@Repository
public interface UserInfoDAOExt {

    List<UserInfo> getUserInfoByKey(String searchKey);
}