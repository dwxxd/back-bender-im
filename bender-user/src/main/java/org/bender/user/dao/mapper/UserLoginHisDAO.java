/* */
package org.bender.user.dao.mapper;

import org.bender.user.dao.entity.UserLoginHis;
import org.bender.user.dao.entity.UserLoginHisCriteria;
import org.springframework.stereotype.Repository;

/**
 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
@Repository
public interface UserLoginHisDAO extends IMapper<UserLoginHis, UserLoginHisCriteria, Long> {
}