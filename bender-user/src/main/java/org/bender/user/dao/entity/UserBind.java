/* */
package org.bender.user.dao.entity;

import java.io.Serializable;
import java.util.Date;

/** 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
public class UserBind implements Serializable {
    /** 
     * 串行版本ID
    */
    private static final long serialVersionUID = 1622606882607512539L;

    /** 
     * 绑定id
     */ 
    private Long id;

    /** 
     */ 
    private Long userId;

    /** 
     * 登录的账号名称
     */ 
    private String userToken;

    /** 
     * 微信的openid
     */ 
    private String wxOpenId;

    /** 
     * 账号的token，类似QQ号
     */ 
    private String accountToken;

    /** 
     * 创建时间
     */ 
    private Date createTime;

    /** 
     * 获取 绑定id user_bind.id
     * @return 绑定id
     */
    public final Long getId() {
        return id;
    }

    /** 
     * 设置 绑定id user_bind.id
     * @param id 绑定id
     */
    public final void setId(Long id) {
        this.id = id;
    }

    /** 
     * 获取 user_bind.user_id
     * @return user_bind.user_id
     */
    public final Long getUserId() {
        return userId;
    }

    /** 
     * 设置 user_bind.user_id
     * @param userId user_bind.user_id
     */
    public final void setUserId(Long userId) {
        this.userId = userId;
    }

    /** 
     * 获取 登录的账号名称 user_bind.user_token
     * @return 登录的账号名称
     */
    public final String getUserToken() {
        return userToken;
    }

    /** 
     * 设置 登录的账号名称 user_bind.user_token
     * @param userToken 登录的账号名称
     */
    public final void setUserToken(String userToken) {
        this.userToken = userToken == null ? null : userToken.trim();
    }

    /** 
     * 获取 微信的openid user_bind.wx_open_id
     * @return 微信的openid
     */
    public final String getWxOpenId() {
        return wxOpenId;
    }

    /** 
     * 设置 微信的openid user_bind.wx_open_id
     * @param wxOpenId 微信的openid
     */
    public final void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId == null ? null : wxOpenId.trim();
    }

    /** 
     * 获取 账号的token，类似QQ号 user_bind.account_token
     * @return 账号的token，类似QQ号
     */
    public final String getAccountToken() {
        return accountToken;
    }

    /** 
     * 设置 账号的token，类似QQ号 user_bind.account_token
     * @param accountToken 账号的token，类似QQ号
     */
    public final void setAccountToken(String accountToken) {
        this.accountToken = accountToken == null ? null : accountToken.trim();
    }

    /** 
     * 获取 创建时间 user_bind.create_time
     * @return 创建时间
     */
    public final Date getCreateTime() {
        return createTime;
    }

    /** 
     * 设置 创建时间 user_bind.create_time
     * @param createTime 创建时间
     */
    public final void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", userToken=").append(userToken);
        sb.append(", wxOpenId=").append(wxOpenId);
        sb.append(", accountToken=").append(accountToken);
        sb.append(", createTime=").append(createTime);
        sb.append("]");
        return sb.toString();
    }
}