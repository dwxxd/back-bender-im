/* */
package org.bender.user.dao.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
public class UserInfoCriteria {
    /** 
     * 排序字段
    */
    protected String orderByClause;

    /** 
     * 过滤重复数据
    */
    protected boolean distinct;

    /** 
     * 查询条件
    */
    protected List<Criteria> oredCriteria;

    /** 
     * 构造查询条件
     */
    public UserInfoCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /** 
     * 设置排序字段
     * @param orderByClause 排序字段
     */
    public final void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /** 
     * 获取排序字段
     */
    public final String getOrderByClause() {
        return orderByClause;
    }

    /** 
     * 设置过滤重复数据
     * @param distinct 是否过滤重复数据
     */
    public final void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /** 
     * 是否过滤重复数据
     */
    public final boolean isDistinct() {
        return distinct;
    }

    /** 
     * 获取当前的查询条件实例
     */
    public final List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /** 
     * 增加或者的查询条件,用于构建或者查询
     * @param criteria 过滤条件实例
     */
    public final void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /** 
     * 创建一个新的或者查询条件
     */
    public final Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /** 
     * 创建一个查询条件
     */
    public final Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /** 
     * 内部构建查询条件对象
     */
    protected final Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /** 
     * 清除查询条件
     */
    public final void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * user_info
     */
    protected abstract static class BaseCriteria {
        protected List<Criterion> criteria;

        protected BaseCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNickNameIsNull() {
            addCriterion("nick_name is null");
            return (Criteria) this;
        }

        public Criteria andNickNameIsNotNull() {
            addCriterion("nick_name is not null");
            return (Criteria) this;
        }

        public Criteria andNickNameEqualTo(String value) {
            addCriterion("nick_name =", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotEqualTo(String value) {
            addCriterion("nick_name <>", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameGreaterThan(String value) {
            addCriterion("nick_name >", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameGreaterThanOrEqualTo(String value) {
            addCriterion("nick_name >=", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameLessThan(String value) {
            addCriterion("nick_name <", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameLessThanOrEqualTo(String value) {
            addCriterion("nick_name <=", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameLike(String value) {
            addCriterion("nick_name like", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotLike(String value) {
            addCriterion("nick_name not like", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameIn(List<String> values) {
            addCriterion("nick_name in", values, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotIn(List<String> values) {
            addCriterion("nick_name not in", values, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameBetween(String value1, String value2) {
            addCriterion("nick_name between", value1, value2, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotBetween(String value1, String value2) {
            addCriterion("nick_name not between", value1, value2, "nickName");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andTagIdsIsNull() {
            addCriterion("tag_ids is null");
            return (Criteria) this;
        }

        public Criteria andTagIdsIsNotNull() {
            addCriterion("tag_ids is not null");
            return (Criteria) this;
        }

        public Criteria andTagIdsEqualTo(String value) {
            addCriterion("tag_ids =", value, "tagIds");
            return (Criteria) this;
        }

        public Criteria andTagIdsNotEqualTo(String value) {
            addCriterion("tag_ids <>", value, "tagIds");
            return (Criteria) this;
        }

        public Criteria andTagIdsGreaterThan(String value) {
            addCriterion("tag_ids >", value, "tagIds");
            return (Criteria) this;
        }

        public Criteria andTagIdsGreaterThanOrEqualTo(String value) {
            addCriterion("tag_ids >=", value, "tagIds");
            return (Criteria) this;
        }

        public Criteria andTagIdsLessThan(String value) {
            addCriterion("tag_ids <", value, "tagIds");
            return (Criteria) this;
        }

        public Criteria andTagIdsLessThanOrEqualTo(String value) {
            addCriterion("tag_ids <=", value, "tagIds");
            return (Criteria) this;
        }

        public Criteria andTagIdsLike(String value) {
            addCriterion("tag_ids like", value, "tagIds");
            return (Criteria) this;
        }

        public Criteria andTagIdsNotLike(String value) {
            addCriterion("tag_ids not like", value, "tagIds");
            return (Criteria) this;
        }

        public Criteria andTagIdsIn(List<String> values) {
            addCriterion("tag_ids in", values, "tagIds");
            return (Criteria) this;
        }

        public Criteria andTagIdsNotIn(List<String> values) {
            addCriterion("tag_ids not in", values, "tagIds");
            return (Criteria) this;
        }

        public Criteria andTagIdsBetween(String value1, String value2) {
            addCriterion("tag_ids between", value1, value2, "tagIds");
            return (Criteria) this;
        }

        public Criteria andTagIdsNotBetween(String value1, String value2) {
            addCriterion("tag_ids not between", value1, value2, "tagIds");
            return (Criteria) this;
        }

        public Criteria andTagCnsIsNull() {
            addCriterion("tag_cns is null");
            return (Criteria) this;
        }

        public Criteria andTagCnsIsNotNull() {
            addCriterion("tag_cns is not null");
            return (Criteria) this;
        }

        public Criteria andTagCnsEqualTo(String value) {
            addCriterion("tag_cns =", value, "tagCns");
            return (Criteria) this;
        }

        public Criteria andTagCnsNotEqualTo(String value) {
            addCriterion("tag_cns <>", value, "tagCns");
            return (Criteria) this;
        }

        public Criteria andTagCnsGreaterThan(String value) {
            addCriterion("tag_cns >", value, "tagCns");
            return (Criteria) this;
        }

        public Criteria andTagCnsGreaterThanOrEqualTo(String value) {
            addCriterion("tag_cns >=", value, "tagCns");
            return (Criteria) this;
        }

        public Criteria andTagCnsLessThan(String value) {
            addCriterion("tag_cns <", value, "tagCns");
            return (Criteria) this;
        }

        public Criteria andTagCnsLessThanOrEqualTo(String value) {
            addCriterion("tag_cns <=", value, "tagCns");
            return (Criteria) this;
        }

        public Criteria andTagCnsLike(String value) {
            addCriterion("tag_cns like", value, "tagCns");
            return (Criteria) this;
        }

        public Criteria andTagCnsNotLike(String value) {
            addCriterion("tag_cns not like", value, "tagCns");
            return (Criteria) this;
        }

        public Criteria andTagCnsIn(List<String> values) {
            addCriterion("tag_cns in", values, "tagCns");
            return (Criteria) this;
        }

        public Criteria andTagCnsNotIn(List<String> values) {
            addCriterion("tag_cns not in", values, "tagCns");
            return (Criteria) this;
        }

        public Criteria andTagCnsBetween(String value1, String value2) {
            addCriterion("tag_cns between", value1, value2, "tagCns");
            return (Criteria) this;
        }

        public Criteria andTagCnsNotBetween(String value1, String value2) {
            addCriterion("tag_cns not between", value1, value2, "tagCns");
            return (Criteria) this;
        }

        public Criteria andCounterIsNull() {
            addCriterion("counter is null");
            return (Criteria) this;
        }

        public Criteria andCounterIsNotNull() {
            addCriterion("counter is not null");
            return (Criteria) this;
        }

        public Criteria andCounterEqualTo(Long value) {
            addCriterion("counter =", value, "counter");
            return (Criteria) this;
        }

        public Criteria andCounterNotEqualTo(Long value) {
            addCriterion("counter <>", value, "counter");
            return (Criteria) this;
        }

        public Criteria andCounterGreaterThan(Long value) {
            addCriterion("counter >", value, "counter");
            return (Criteria) this;
        }

        public Criteria andCounterGreaterThanOrEqualTo(Long value) {
            addCriterion("counter >=", value, "counter");
            return (Criteria) this;
        }

        public Criteria andCounterLessThan(Long value) {
            addCriterion("counter <", value, "counter");
            return (Criteria) this;
        }

        public Criteria andCounterLessThanOrEqualTo(Long value) {
            addCriterion("counter <=", value, "counter");
            return (Criteria) this;
        }

        public Criteria andCounterIn(List<Long> values) {
            addCriterion("counter in", values, "counter");
            return (Criteria) this;
        }

        public Criteria andCounterNotIn(List<Long> values) {
            addCriterion("counter not in", values, "counter");
            return (Criteria) this;
        }

        public Criteria andCounterBetween(Long value1, Long value2) {
            addCriterion("counter between", value1, value2, "counter");
            return (Criteria) this;
        }

        public Criteria andCounterNotBetween(Long value1, Long value2) {
            addCriterion("counter not between", value1, value2, "counter");
            return (Criteria) this;
        }

        public Criteria andCityIsNull() {
            addCriterion("city is null");
            return (Criteria) this;
        }

        public Criteria andCityIsNotNull() {
            addCriterion("city is not null");
            return (Criteria) this;
        }

        public Criteria andCityEqualTo(Long value) {
            addCriterion("city =", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotEqualTo(Long value) {
            addCriterion("city <>", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThan(Long value) {
            addCriterion("city >", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThanOrEqualTo(Long value) {
            addCriterion("city >=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThan(Long value) {
            addCriterion("city <", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThanOrEqualTo(Long value) {
            addCriterion("city <=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityIn(List<Long> values) {
            addCriterion("city in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotIn(List<Long> values) {
            addCriterion("city not in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityBetween(Long value1, Long value2) {
            addCriterion("city between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotBetween(Long value1, Long value2) {
            addCriterion("city not between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andRegionIsNull() {
            addCriterion("region is null");
            return (Criteria) this;
        }

        public Criteria andRegionIsNotNull() {
            addCriterion("region is not null");
            return (Criteria) this;
        }

        public Criteria andRegionEqualTo(Long value) {
            addCriterion("region =", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionNotEqualTo(Long value) {
            addCriterion("region <>", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionGreaterThan(Long value) {
            addCriterion("region >", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionGreaterThanOrEqualTo(Long value) {
            addCriterion("region >=", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionLessThan(Long value) {
            addCriterion("region <", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionLessThanOrEqualTo(Long value) {
            addCriterion("region <=", value, "region");
            return (Criteria) this;
        }

        public Criteria andRegionIn(List<Long> values) {
            addCriterion("region in", values, "region");
            return (Criteria) this;
        }

        public Criteria andRegionNotIn(List<Long> values) {
            addCriterion("region not in", values, "region");
            return (Criteria) this;
        }

        public Criteria andRegionBetween(Long value1, Long value2) {
            addCriterion("region between", value1, value2, "region");
            return (Criteria) this;
        }

        public Criteria andRegionNotBetween(Long value1, Long value2) {
            addCriterion("region not between", value1, value2, "region");
            return (Criteria) this;
        }

        public Criteria andLocationCnIsNull() {
            addCriterion("location_cn is null");
            return (Criteria) this;
        }

        public Criteria andLocationCnIsNotNull() {
            addCriterion("location_cn is not null");
            return (Criteria) this;
        }

        public Criteria andLocationCnEqualTo(String value) {
            addCriterion("location_cn =", value, "locationCn");
            return (Criteria) this;
        }

        public Criteria andLocationCnNotEqualTo(String value) {
            addCriterion("location_cn <>", value, "locationCn");
            return (Criteria) this;
        }

        public Criteria andLocationCnGreaterThan(String value) {
            addCriterion("location_cn >", value, "locationCn");
            return (Criteria) this;
        }

        public Criteria andLocationCnGreaterThanOrEqualTo(String value) {
            addCriterion("location_cn >=", value, "locationCn");
            return (Criteria) this;
        }

        public Criteria andLocationCnLessThan(String value) {
            addCriterion("location_cn <", value, "locationCn");
            return (Criteria) this;
        }

        public Criteria andLocationCnLessThanOrEqualTo(String value) {
            addCriterion("location_cn <=", value, "locationCn");
            return (Criteria) this;
        }

        public Criteria andLocationCnLike(String value) {
            addCriterion("location_cn like", value, "locationCn");
            return (Criteria) this;
        }

        public Criteria andLocationCnNotLike(String value) {
            addCriterion("location_cn not like", value, "locationCn");
            return (Criteria) this;
        }

        public Criteria andLocationCnIn(List<String> values) {
            addCriterion("location_cn in", values, "locationCn");
            return (Criteria) this;
        }

        public Criteria andLocationCnNotIn(List<String> values) {
            addCriterion("location_cn not in", values, "locationCn");
            return (Criteria) this;
        }

        public Criteria andLocationCnBetween(String value1, String value2) {
            addCriterion("location_cn between", value1, value2, "locationCn");
            return (Criteria) this;
        }

        public Criteria andLocationCnNotBetween(String value1, String value2) {
            addCriterion("location_cn not between", value1, value2, "locationCn");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andBackImgFileIdIsNull() {
            addCriterion("back_img_file_id is null");
            return (Criteria) this;
        }

        public Criteria andBackImgFileIdIsNotNull() {
            addCriterion("back_img_file_id is not null");
            return (Criteria) this;
        }

        public Criteria andBackImgFileIdEqualTo(Long value) {
            addCriterion("back_img_file_id =", value, "backImgFileId");
            return (Criteria) this;
        }

        public Criteria andBackImgFileIdNotEqualTo(Long value) {
            addCriterion("back_img_file_id <>", value, "backImgFileId");
            return (Criteria) this;
        }

        public Criteria andBackImgFileIdGreaterThan(Long value) {
            addCriterion("back_img_file_id >", value, "backImgFileId");
            return (Criteria) this;
        }

        public Criteria andBackImgFileIdGreaterThanOrEqualTo(Long value) {
            addCriterion("back_img_file_id >=", value, "backImgFileId");
            return (Criteria) this;
        }

        public Criteria andBackImgFileIdLessThan(Long value) {
            addCriterion("back_img_file_id <", value, "backImgFileId");
            return (Criteria) this;
        }

        public Criteria andBackImgFileIdLessThanOrEqualTo(Long value) {
            addCriterion("back_img_file_id <=", value, "backImgFileId");
            return (Criteria) this;
        }

        public Criteria andBackImgFileIdIn(List<Long> values) {
            addCriterion("back_img_file_id in", values, "backImgFileId");
            return (Criteria) this;
        }

        public Criteria andBackImgFileIdNotIn(List<Long> values) {
            addCriterion("back_img_file_id not in", values, "backImgFileId");
            return (Criteria) this;
        }

        public Criteria andBackImgFileIdBetween(Long value1, Long value2) {
            addCriterion("back_img_file_id between", value1, value2, "backImgFileId");
            return (Criteria) this;
        }

        public Criteria andBackImgFileIdNotBetween(Long value1, Long value2) {
            addCriterion("back_img_file_id not between", value1, value2, "backImgFileId");
            return (Criteria) this;
        }

        public Criteria andHeadImgFileIdIsNull() {
            addCriterion("head_img_file_id is null");
            return (Criteria) this;
        }

        public Criteria andHeadImgFileIdIsNotNull() {
            addCriterion("head_img_file_id is not null");
            return (Criteria) this;
        }

        public Criteria andHeadImgFileIdEqualTo(Long value) {
            addCriterion("head_img_file_id =", value, "headImgFileId");
            return (Criteria) this;
        }

        public Criteria andHeadImgFileIdNotEqualTo(Long value) {
            addCriterion("head_img_file_id <>", value, "headImgFileId");
            return (Criteria) this;
        }

        public Criteria andHeadImgFileIdGreaterThan(Long value) {
            addCriterion("head_img_file_id >", value, "headImgFileId");
            return (Criteria) this;
        }

        public Criteria andHeadImgFileIdGreaterThanOrEqualTo(Long value) {
            addCriterion("head_img_file_id >=", value, "headImgFileId");
            return (Criteria) this;
        }

        public Criteria andHeadImgFileIdLessThan(Long value) {
            addCriterion("head_img_file_id <", value, "headImgFileId");
            return (Criteria) this;
        }

        public Criteria andHeadImgFileIdLessThanOrEqualTo(Long value) {
            addCriterion("head_img_file_id <=", value, "headImgFileId");
            return (Criteria) this;
        }

        public Criteria andHeadImgFileIdIn(List<Long> values) {
            addCriterion("head_img_file_id in", values, "headImgFileId");
            return (Criteria) this;
        }

        public Criteria andHeadImgFileIdNotIn(List<Long> values) {
            addCriterion("head_img_file_id not in", values, "headImgFileId");
            return (Criteria) this;
        }

        public Criteria andHeadImgFileIdBetween(Long value1, Long value2) {
            addCriterion("head_img_file_id between", value1, value2, "headImgFileId");
            return (Criteria) this;
        }

        public Criteria andHeadImgFileIdNotBetween(Long value1, Long value2) {
            addCriterion("head_img_file_id not between", value1, value2, "headImgFileId");
            return (Criteria) this;
        }
    }

    /**
     * user_info的映射实体
     */
    public static class Criteria extends BaseCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * user_info的动态SQL对象.
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}