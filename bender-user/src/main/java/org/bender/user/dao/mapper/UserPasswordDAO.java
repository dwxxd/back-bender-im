/* */
package org.bender.user.dao.mapper;

import org.bender.user.dao.entity.UserPassword;
import org.bender.user.dao.entity.UserPasswordCriteria;
import org.springframework.stereotype.Repository;

/**
 
 * @author CodeBender
 * date:2021/02/19 17:45
 */
@Repository
public interface UserPasswordDAO extends IMapper<UserPassword, UserPasswordCriteria, Long> {
}