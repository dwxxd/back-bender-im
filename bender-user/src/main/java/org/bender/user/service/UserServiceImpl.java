package org.bender.user.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.bender.api.user.dto.UserBindDto;
import org.bender.api.user.dto.UserInfoDto;
import org.bender.api.user.dto.UserPasswordDto;
import org.bender.common.PageParam;
import org.bender.common.Result;
import org.bender.enums.CommonEnum;
import org.bender.user.dao.entity.*;
import org.bender.user.dao.mapper.UserBindDAO;
import org.bender.user.dao.mapper.UserInfoDAO;
import org.bender.user.dao.mapper.UserInfoDAOExt;
import org.bender.user.dao.mapper.UserPasswordDAO;
import org.bender.util.BeanUtils;
import org.bender.util.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author CodeBender-zhj
 * @date 2021/2/25
 **/
@Service
@Component
@Slf4j
public class UserServiceImpl implements UserService {


    @Autowired
    private UserInfoDAO userInfoDAO;

    @Autowired
    private UserPasswordDAO userPasswordDAO;

    @Autowired
    private UserBindDAO userBindDAO;


    @Autowired
    private UserInfoDAOExt userInfoDAOExt;

    @Override
    @Transactional
    public UserInfoDto insertOrUpdateUserInfo(UserInfoDto userInfoDto) {
        UserInfo userInfo = new UserInfo();
        try {
            if (null == userInfoDto.getId()) {
                userInfoDto.setCreateTime(new Date());
                BeanUtils.copyProperties(userInfo, userInfoDto);
                userInfoDAO.insertSelective(userInfo);
                userInfoDto.setId(userInfo.getId());
            } else {
                userInfoDAO.updateByPrimaryKeySelective(userInfo);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return userInfoDto;
    }

    @Override
    @Transactional
    public UserPasswordDto insertOrUpdateUserPwd(UserPasswordDto userPasswordDto) {
        UserPassword userPassword = new UserPassword();
        try {
            //将之前的密码设置为不能用
            UserPassword oldPwd = new UserPassword();
            oldPwd.setStatus(CommonEnum.DELETE.getCode());
            UserPasswordCriteria ex = new UserPasswordCriteria();
            UserPasswordCriteria.Criteria criteria = ex.createCriteria();
            criteria.andUserIdEqualTo(userPasswordDto.getUserId());
            userPasswordDAO.updateByExampleSelective(oldPwd, ex);
            //创建一个有效的密码
            userPasswordDto.setCreateTime(new Date());
            BeanUtils.copyProperties(userPassword, userPasswordDto);
            userPassword.setPassword(MD5Utils.toMD5(userPasswordDto.getPassword()));
            userPassword.setStatus(CommonEnum.NOT_DELETE.getCode());
            userPasswordDAO.insertSelective(userPassword);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return userPasswordDto;
    }

    @Override
    public UserBindDto insertOrUpdateUserBind(UserBindDto userBindDto) {
        UserBind userBind = new UserBind();
        try {
            if (null == userBindDto.getId()) {
                userBindDto.setAccountToken(UUID.randomUUID().toString());
                userBindDto.setCreateTime(new Date());
                BeanUtils.copyProperties(userBind, userBindDto);
                userBindDAO.insertSelective(userBind);
            } else {
                userBindDAO.updateByPrimaryKeySelective(userBind);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return userBindDto;
    }

    @Override
    public UserBindDto selectUserBindDto(Long userId, String userToken, String accountToken, String wxOpenId) {

        UserBindCriteria ex = new UserBindCriteria();
        UserBindCriteria.Criteria criteria = ex.createCriteria();
        if (null != userId) {
            criteria.andUserIdEqualTo(userId);
        }
        if (!StringUtils.isEmpty(userToken)) {
            criteria.andUserTokenEqualTo(userToken);
        }
        if (!StringUtils.isEmpty(accountToken)) {
            criteria.andAccountTokenEqualTo(accountToken);
        }
        if (!StringUtils.isEmpty(wxOpenId)) {
            criteria.andWxOpenIdEqualTo(wxOpenId);
        }
        List<UserBind> userBinds = userBindDAO.selectByExample(ex);
        if (CollectionUtils.isNotEmpty(userBinds)) {
            try {
                UserBindDto userBindDto = new UserBindDto();
                BeanUtils.copyProperties(userBindDto, userBinds.get(0));
                return userBindDto;
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return null;
    }

    @Override
    public Boolean checkPwd(Long userId, String pwd) {
        UserPasswordCriteria ex = new UserPasswordCriteria();
        ex.createCriteria().andUserIdEqualTo(userId).andStatusEqualTo(CommonEnum.NOT_DELETE.getCode()).andPasswordEqualTo(MD5Utils.toMD5(pwd));
        List<UserPassword> userPasswords = userPasswordDAO.selectByExample(ex);
        return CollectionUtils.isNotEmpty(userPasswords);
    }

    @Override
    public UserInfoDto selectUserInfoByUserId(Long userId) {
        UserInfo userInfo = userInfoDAO.selectByPrimaryKey(userId);
        try {
            UserInfoDto userInfoDto = new UserInfoDto();
            BeanUtils.copyProperties(userInfoDto, userInfo);
            return userInfoDto;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public Result<PageInfo<UserInfoDto>> getUserInfoByKeyPage(PageParam pageNum, Long comId) {
        PageInfo<UserInfoDto> result = new PageInfo<>();
        PageHelper.startPage(pageNum.getPageNum(), pageNum.getPageSize());
        List<UserInfo> userInfoByKey = userInfoDAOExt.getUserInfoByKey(pageNum.getSearchKey());
        if (CollectionUtils.isNotEmpty(userInfoByKey)) {
            PageInfo<UserInfo> userInfoPageInfo = new PageInfo<>(userInfoByKey);
            List<UserInfo> list = userInfoPageInfo.getList();
            List<UserInfoDto> userInfoDtos = BeanUtils.copyListProperties(UserInfoDto.class, list);
            userInfoPageInfo.setList(null);
            try {
                BeanUtils.copyProperties(result, userInfoPageInfo);
                result.setList(userInfoDtos);
            } catch (Exception e) {
            }
        }
        return Result.success(result);
    }
}
