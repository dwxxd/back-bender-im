package org.bender.user.service;

import com.github.pagehelper.PageInfo;
import org.bender.api.user.dto.UserBindDto;
import org.bender.api.user.dto.UserInfoDto;
import org.bender.api.user.dto.UserPasswordDto;
import org.bender.common.PageParam;
import org.bender.common.Result;

/**
 * @author CodeBender-zhj
 * @date 2021/2/25
 **/
public interface UserService {

    UserBindDto insertOrUpdateUserBind(UserBindDto userBindDto);

    UserInfoDto insertOrUpdateUserInfo(UserInfoDto userInfoDto);

    UserPasswordDto  insertOrUpdateUserPwd(UserPasswordDto userPasswordDto);

    UserBindDto selectUserBindDto(Long userId,String userToken,String accountToken,String wxOpenId);

    Boolean checkPwd (Long userId, String pwd);

    UserInfoDto selectUserInfoByUserId(Long userId);

    Result<PageInfo<UserInfoDto>> getUserInfoByKeyPage(PageParam pageNum, Long comId);
}
