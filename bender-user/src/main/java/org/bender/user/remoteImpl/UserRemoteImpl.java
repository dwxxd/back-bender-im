package org.bender.user.remoteImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.bender.api.user.UserRemote;
import org.bender.api.user.dto.UserBindDto;
import org.bender.api.user.dto.UserInfoDto;
import org.bender.api.user.dto.UserPasswordDto;
import org.bender.api.user.req.CommonReq;
import org.bender.common.PageParam;
import org.bender.common.Result;
import org.bender.common.ResultStatus;
import org.bender.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author CodeBender-zhj
 * @date 2021/2/25
 **/
@Service
@Component
@Slf4j
public class UserRemoteImpl implements UserRemote {

    @Autowired
    private UserService userService;

    @Override
    public Result<UserInfoDto> registerUser(UserInfoDto userInfoDto) {
        //验证userToken是否重复
        UserBindDto checkBind = userService.selectUserBindDto(null, userInfoDto.getUserBindDto().getUserToken(), null, null);
        if (null != checkBind) {
            return Result.resultStatus(ResultStatus.user_exist);
        }
        try {
            userInfoDto = userService.insertOrUpdateUserInfo(userInfoDto);
            UserPasswordDto userPasswordDto = userInfoDto.getUserPasswordDto();
            userPasswordDto.setUserId(userInfoDto.getId());
            userService.insertOrUpdateUserPwd(userPasswordDto);
            userInfoDto.setId(userInfoDto.getId());
            UserBindDto userBindDto = userInfoDto.getUserBindDto();
            userBindDto.setUserId(userInfoDto.getId());
            userService.insertOrUpdateUserBind(userBindDto);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Result.success(userInfoDto);
    }

    @Override
    public Result<UserInfoDto> login(UserBindDto userBindDto, String pwd) {
        UserBindDto checkBind = userService.selectUserBindDto(null, userBindDto.getUserToken(), userBindDto.getAccountToken(), userBindDto.getWxOpenId());
        if (null == checkBind) {
            return Result.resultStatus(ResultStatus.user_not_exist);
        }
        if (StringUtils.isNotEmpty(userBindDto.getUserToken()) && StringUtils.isEmpty(userBindDto.getAccountToken()) && StringUtils.isEmpty(userBindDto.getWxOpenId())) {
            Boolean aBoolean = userService.checkPwd(checkBind.getUserId(), pwd);
            if (!aBoolean) {
                return Result.resultStatus(ResultStatus.pwd_not_exist);
            }
        }
        UserInfoDto userInfoDto = userService.selectUserInfoByUserId(checkBind.getUserId());
        userInfoDto.setSessionKey(checkBind.getAccountToken());
        return Result.success(userInfoDto);
    }

    @Override
    public Result<UserInfoDto> selectByUserToken(String userToken) {
        UserBindDto checkBind = userService.selectUserBindDto(null, userToken, null, null);
        if (null == checkBind) {
            return Result.fail(ResultStatus.user_not_exist);
        }
        return Result.success(userService.selectUserInfoByUserId(checkBind.getUserId()));
    }

    @Override
    public Result addFriend(CommonReq req) {
        return null;
    }

    @Override
    public Result<PageInfo<UserInfoDto>> getUserInfoByKeyPage(PageParam pageNum, Long comId) {
        return userService.getUserInfoByKeyPage(pageNum, comId);
    }
}
