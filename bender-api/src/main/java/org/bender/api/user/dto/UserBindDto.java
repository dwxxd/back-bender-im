package org.bender.api.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * @author CodeBender-zhj
 * @date 2021/2/25
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserBindDto implements Serializable {
    private static final long serialVersionUID = -900304352598068045L;

    /**
     * 绑定id
     */
    private Long id;

    /**
     */
    private Long userId;

    /**
     * 登录的账号名称
     */
    @NotBlank(message = "用户名不能为空")
    private String userToken;

    /**
     * 微信的openid
     */
    private String wxOpenId;

    /**
     * 账号的token，类似QQ号
     */
    private String accountToken;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
