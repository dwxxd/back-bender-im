package org.bender.api.user.dto;

import lombok.Data;
import lombok.NonNull;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author CodeBender-zhj
 * @date 2020/7/28
 **/
@Data
public class UserInfoDto implements Serializable {
    private static final long serialVersionUID = -767460578142065713L;

    /**
     * 用户id
     */
    private Long id;

    private String sessionKey;

    /**
     * 昵称
     */
    @NotBlank(message = "昵称不能为空")
    private String nickName;

    /**
     * 手机
     */
    private String phone;

    /**
     *
     */
    private String tagIds;

    /**
     *
     */
    private String tagCns;

    /**
     *
     */
    private Long counter;

    /**
     *
     */
    private Long city;

    /**
     *
     */
    private Long region;

    /**
     *
     */
    private String locationCn;

    /**
     * 用户状态 0正常 1 被禁用  默认：0
     */
    private Integer status;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 背景图照片
     */
    private Long backImgFileId;

    /**
     * 头像的文件id
     */
    private Long headImgFileId;
    private String headImgFileUrl;

    /**
     *
     */
    private String remark;

    /**
     * 密码dto
     */
    @NotNull(message = "密码不能为空")
    private UserPasswordDto userPasswordDto;

    /**
     * 绑定dto
     */
    @NotNull(message = "密用户名不能为空")
    private UserBindDto userBindDto;

}
