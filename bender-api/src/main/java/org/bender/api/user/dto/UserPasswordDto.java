package org.bender.api.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * @author CodeBender-zhj
 * @date 2021/2/25
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserPasswordDto implements Serializable {
    private static final long serialVersionUID = -6519348722853119870L;


    /**
     */
    private Long id;

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空")
    private String password;

    /**
     * 状态，0无效 1有效
     */
    private Integer status;

    /**
     */
    private Long userId;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
