package org.bender.api.user;

import com.github.pagehelper.PageInfo;
import org.bender.api.user.dto.UserBindDto;
import org.bender.api.user.dto.UserInfoDto;
import org.bender.api.user.req.CommonReq;
import org.bender.common.PageParam;
import org.bender.common.Result;

/**
 * @author CodeBender-zhj
 * @date 2020/7/28
 **/
public interface UserRemote {

    /**
     * 注册
     *
     * @param userInfoDto
     * @return
     */
    Result<UserInfoDto> registerUser(UserInfoDto userInfoDto);

    /**
     * 登录，支持手机号、账户
     * @param userBindDto
     * @param pwd
     * @return
     */
    Result<UserInfoDto> login(UserBindDto userBindDto, String pwd);

    /**
     * 获取用户基本信息
     * @param userToken
     * @return
     */
    Result<UserInfoDto>  selectByUserToken(String userToken);

    /**
     * 加好友
     * @param req
     * @return
     */
    Result addFriend(CommonReq req);

    /**
     * 更具accountToken 和昵称 手机号 模糊查询用户
     * @param pageNum
     * @param comId
     * @return
     */
    Result<PageInfo<UserInfoDto>> getUserInfoByKeyPage(PageParam pageNum, Long comId);
}
