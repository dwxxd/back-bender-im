package org.bender.api.user.req;

import lombok.Data;

import java.io.Serializable;

/**
 * @author CodeBender-zhj
 * @date 2020/8/10
 **/

@Data
public class CommonReq implements Serializable {
    private static final long serialVersionUID = 3200157962814052203L;

    private Long userId;
    private String accountNum;
    private Long toUserId;
    private String toAccountNum;
    private String remark;
    private String nickName;
    private String uuid;
}
