package org.bender.api.file.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author CodeBender
 * @date 2020/7/16
 * 世上无难事只怕有心人
 **/
@Data
public class FileDto implements Serializable {
    private static final long serialVersionUID = -215410564455525681L;

    private Long fileId;

    private String fileName;

    private String url;

    private Long creater;

    private Long createTime;

    private String uuid;

    private Long size;

    private Long timeSize;
    private String truePath;
    private String truePort;
}
