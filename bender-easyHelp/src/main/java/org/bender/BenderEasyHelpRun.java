package org.bender;


import org.bender.util.BeanUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages={"org.bender"})
@MapperScan(basePackages = "org.bender.easyhelp.dao.mapper")
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class BenderEasyHelpRun {

    public static void main(String[] args) throws Exception{
        ConfigurableApplicationContext run = SpringApplication.run(BenderEasyHelpRun.class, args);
		//将run方法的返回值赋值给工具类中的静态变量
        BeanUtils.applicationContext = run;
    }


}

