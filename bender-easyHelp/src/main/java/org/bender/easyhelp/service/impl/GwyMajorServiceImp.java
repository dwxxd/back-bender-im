package org.bender.easyhelp.service.impl;

import org.apache.commons.lang.StringUtils;
import org.bender.easyhelp.dao.entity.GwyMajorMain;
import org.bender.easyhelp.dao.entity.GwyMajorMainCriteria;
import org.bender.easyhelp.dao.entity.GwyMajorType;
import org.bender.easyhelp.dao.entity.GwyMajorTypeCriteria;
import org.bender.easyhelp.dao.mapper.GwyMajorMainDAO;
import org.bender.easyhelp.dao.mapper.GwyMajorTypeDAO;
import org.bender.easyhelp.service.GwyMajorService;
import org.bender.enums.CommonEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GwyMajorServiceImp implements GwyMajorService {

    @Autowired
    private GwyMajorMainDAO gwyMajorMainDAO;

    @Autowired
    private GwyMajorTypeDAO gwyMajorTypeDAO;

    @Override
    public List<GwyMajorMain> selectGwyMajorBykey(String key, Integer topStatus, Integer typeId) {
        GwyMajorMainCriteria ex = new GwyMajorMainCriteria();
        GwyMajorMainCriteria.Criteria criteria = ex.createCriteria();
        criteria.andStatusEqualTo(CommonEnum.NOT_DELETE.getCode());
        if (null != typeId) {
            criteria.andTypeIdEqualTo(typeId);
        }
        if (null != topStatus) {
            criteria.andTopStatusEqualTo(topStatus);
        }
        if (StringUtils.isNotBlank(key)) {
            criteria.andMajorNameEqualTo(key);
        }
        return gwyMajorMainDAO.selectByExample(ex);
    }


    @Override
    public List<GwyMajorMain> selectGwyMajorBykeyLike(String key, Integer topStatus, Integer typeId) {
        GwyMajorMainCriteria ex = new GwyMajorMainCriteria();
        GwyMajorMainCriteria.Criteria criteria = ex.createCriteria();
        criteria.andStatusEqualTo(CommonEnum.NOT_DELETE.getCode());
        if (null != typeId) {
            criteria.andTypeIdEqualTo(typeId);
        }
        if (null != topStatus) {
            criteria.andTopStatusEqualTo(topStatus);
        }
        if (StringUtils.isNotBlank(key)) {
            criteria.andMajorNameLike("%" + key + "%");
        }
        return gwyMajorMainDAO.selectByExample(ex);
    }

    @Override
    @Transactional
    public Boolean insertGwyMajorMain(List<GwyMajorMain> majorMains) {
        return gwyMajorMainDAO.insertBatchSelective(majorMains) > 0;
    }

    @Override
    @Transactional
    public GwyMajorMain insertGwyMajorMainFeedBack(GwyMajorMain gwyMajorMain) {
        gwyMajorMainDAO.insertSelective(gwyMajorMain);
        return gwyMajorMain;
    }

    @Override
    @Transactional
    public Boolean updateGwyMajorMain(List<GwyMajorMain> majorMains) {
        return gwyMajorMainDAO.updateBatchByPrimaryKeySelective(majorMains) > 0;
    }

    @Override
    public List<GwyMajorType> selectType() {
        GwyMajorTypeCriteria gwyMajorTypeCriteria = new GwyMajorTypeCriteria();
        gwyMajorTypeCriteria.createCriteria().andStatusEqualTo(CommonEnum.NOT_DELETE.getCode());
        return gwyMajorTypeDAO.selectByExample(gwyMajorTypeCriteria);
    }
}
