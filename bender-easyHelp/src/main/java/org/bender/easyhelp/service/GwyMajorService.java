package org.bender.easyhelp.service;

import org.bender.easyhelp.dao.entity.GwyMajorMain;
import org.bender.easyhelp.dao.entity.GwyMajorType;

import java.util.List;

public interface GwyMajorService {

    List<GwyMajorMain> selectGwyMajorBykey(String key,Integer topStatus,Integer typeId);
    List<GwyMajorMain> selectGwyMajorBykeyLike(String key,Integer topStatus,Integer typeId);


    Boolean insertGwyMajorMain(List<GwyMajorMain> majorMains);

    GwyMajorMain insertGwyMajorMainFeedBack(GwyMajorMain gwyMajorMain);

    Boolean updateGwyMajorMain(List<GwyMajorMain> majorMains);


    List<GwyMajorType> selectType();
}
