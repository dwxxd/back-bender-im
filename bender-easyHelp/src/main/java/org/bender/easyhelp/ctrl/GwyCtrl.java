package org.bender.easyhelp.ctrl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.bender.common.Result;
import org.bender.easyhelp.dao.entity.GwyMajorMain;
import org.bender.easyhelp.service.GwyMajorService;
import org.bender.enums.CommonEnum;
import org.bender.util.RedisUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author CodeBender-zhj
 * @date 2020/7/28
 **/
@Controller
@RequestMapping(value = "/gwy")
@Slf4j
public class GwyCtrl {


    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private GwyMajorService gwyMajorService;

    private String keyHead = "GwyMajorBykeyv3-";


    @RequestMapping(value = "/public/selectGwyMajorBykey.api")
    @ResponseBody
    public Result<List<GwyMajorMain>> selectGwyMajorBykey(String key, Integer topStatus, Integer typeId) {
        if (null != topStatus && topStatus == 1) {
            Object o = redisUtil.get(keyHead + typeId);
            if (null == o) {
                List<GwyMajorMain> gwyMajorMains = gwyMajorService.selectGwyMajorBykey(null, null, typeId);
                if (!CollectionUtils.isEmpty(gwyMajorMains)) {
                    List<GwyMajorMain> gwyMajorMainTree = getGwyMajorMainTree(gwyMajorMains, typeId);
                    return Result.success(gwyMajorMainTree);
                }
            } else {
                return Result.success(o);
            }
        }
        return Result.success(gwyMajorService.selectGwyMajorBykeyLike(key, topStatus, typeId));
    }

    @RequestMapping(value = "/public/selectGwyMajorTreeById.api")
    @ResponseBody
    public Result selectGwyMajorTreeById(String idsStr, Integer typeId) {
        Map<Integer, Integer> idsMap = new HashMap<>();
        if (StringUtils.isNotBlank(idsStr)) {
            for (String item : idsStr.split(",")) {
                if (StringUtils.isNotBlank(item)) {
                    idsMap.put(Integer.valueOf(item), 1);
                }
            }
        }
        Result<List<GwyMajorMain>> result = selectGwyMajorBykey(null, 1, typeId);
        List<GwyMajorMain> data = result.getData();
        List<GwyMajorMain> back = new ArrayList<>();
        if (!CollectionUtils.isEmpty(data) && !CollectionUtils.isEmpty(idsMap)) {
            back = data.stream().filter(item -> null != idsMap.get(item.getId()) && idsMap.get(item.getId()) == 1).collect(Collectors.toList());
        }
        return Result.success(back);
    }

    private List<GwyMajorMain> getGwyMajorMainTree(List<GwyMajorMain> gwyMajorMains, Integer typeId) {
        Map<Integer, GwyMajorMain> top = new HashMap<>();
        Multimap<Integer, GwyMajorMain> map = ArrayListMultimap.create();
        gwyMajorMains.stream().forEach(item -> {
            if (null != item.getTopStatus() && item.getTopStatus() == 1) {
                top.put(item.getId(), item);
            } else {
                String parentIds = item.getParentIds();
                if (StringUtils.isNotBlank(parentIds)) {
                    Arrays.stream(parentIds.split(",")).forEach(child -> {
                        if (null != child && StringUtils.isNotBlank(child.trim())) {
                            map.put(Integer.valueOf(child.trim()), item);
                        }
                    });
                }
            }
        });
        List<GwyMajorMain> result = new ArrayList<>();
        if (!top.isEmpty() && !map.isEmpty()) {
            top.forEach((key, value) -> {
                if (null != map.get(key)) {
                    value.setChilds((List<GwyMajorMain>) map.get(key));
                    result.add(value);
                }
            });
            redisUtil.set(keyHead + typeId, result, 60 * 60 * 24);
        }
        return result;
    }

    private Integer yjs = 3;
    private Integer bk = 1;
    private Integer zk = 2;

    @RequestMapping(value = "/public/jsoup.api")
    @ResponseBody
    public Result jsoup(String html) throws Exception {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        Document html1 = Jsoup.parse(html);
        Elements a = html1.getElementsByTag("tr");
        for (Element link : a) {
            Element child = link.child(0);
            String parentName = child.text().trim().split("）")[1];//分类
            Element child1 = link.child(1);
            String m1 = child1.text().trim().replace(",", "，");//研究生
            Element child2 = link.child(2);
            String m2 = child2.text().trim().replace(",", "，");//本科
            Element child3 = link.child(3);
            String m3 = child3.text().trim().replace(",", "，");//专科
            if (StringUtils.isNotBlank(m1)) {
                int typeId = yjs;
                setMajore(year, parentName, m1, typeId);
            }
            if (StringUtils.isNotBlank(m2)) {
                int typeId = bk;
                setMajore(year, parentName, m2, typeId);
            }
            if (StringUtils.isNotBlank(m3)) {
                int typeId = zk;
                setMajore(year, parentName, m3, typeId);
            }
        }
        return Result.success();
    }

    private void setMajore(int year, String parentName, String m1, int typeId) {
        GwyMajorMain gwyMajorMain = new GwyMajorMain();
        gwyMajorMain.setMajorName(parentName);
        gwyMajorMain.setParentIds("," + 0 + ",");
        gwyMajorMain.setStatus(CommonEnum.NOT_DELETE.getCode());
        gwyMajorMain.setTopStatus(1);
        gwyMajorMain.setTypeId(typeId);
        gwyMajorMain.setYear(year);
        gwyMajorService.insertGwyMajorMainFeedBack(gwyMajorMain);
        List<GwyMajorMain> batchInsert = new ArrayList<>();
        List<GwyMajorMain> batchUpdate = new ArrayList<>();
        Map<String, Boolean> check = new HashMap<>();
        for (String s : m1.split("，")) {
            if (StringUtils.isNotBlank(s)) {
                if (null == check.get(s) || !check.get(s)) {
                    check.put(s, true);
                    List<GwyMajorMain> gwyMajorMains1 = gwyMajorService.selectGwyMajorBykey(s, 0, typeId);
                    GwyMajorMain temp;
                    if (CollectionUtils.isEmpty(gwyMajorMains1)) {
                        temp = new GwyMajorMain();
                        temp.setMajorName(s);
                        temp.setParentIds("," + gwyMajorMain.getId() + ",");
                        temp.setStatus(CommonEnum.NOT_DELETE.getCode());
                        temp.setTopStatus(0);
                        temp.setTypeId(typeId);
                        temp.setYear(year);
                        batchInsert.add(temp);
                    } else {
                        temp = gwyMajorMains1.get(0);
                        temp.setParentIds(temp.getParentIds() + gwyMajorMain.getId() + ",");
                        batchUpdate.add(temp);
                    }
                }
            }
        }
        if (!CollectionUtils.isEmpty(batchInsert)) {
            gwyMajorService.insertGwyMajorMain(batchInsert);
        }
        if (!CollectionUtils.isEmpty(batchUpdate)) {
            gwyMajorService.updateGwyMajorMain(batchUpdate);
        }
    }

    @RequestMapping(value = "/public/insertGwyMajorMain.api")
    @ResponseBody
    public Result insertGwyMajorMain(String parentName, String[] majors, Integer[] typeIds) {
        if (StringUtils.isNotBlank(parentName) && null != majors && majors.length > 0 && null != typeIds && typeIds.length > 0) {
            int year = Calendar.getInstance().get(Calendar.YEAR);
            for (int i = 0; i < typeIds.length; i++) {
                List<GwyMajorMain> batchInsert = new ArrayList<>();
                List<GwyMajorMain> batchUpdate = new ArrayList<>();
                String major = majors[i].trim();
                Integer typeId = typeIds[i];
                List<GwyMajorMain> gwyMajorMains = gwyMajorService.selectGwyMajorBykey(parentName, 1, typeIds[i]);
                GwyMajorMain gwyMajorMain;
                if (!CollectionUtils.isEmpty(gwyMajorMains)) {
                    if (gwyMajorMains.size() == 1) {
                        gwyMajorMain = gwyMajorMains.get(0);
                    } else {
                        return Result.fail("多个一级：" + parentName);
                    }
                } else {
                    gwyMajorMain = new GwyMajorMain();
                    gwyMajorMain.setMajorName(parentName);
                    gwyMajorMain.setParentIds("," + 0 + ",");
                    gwyMajorMain.setStatus(CommonEnum.NOT_DELETE.getCode());
                    gwyMajorMain.setTopStatus(1);
                    gwyMajorMain.setTypeId(typeId);
                    gwyMajorMain.setYear(year);
                    gwyMajorMain = gwyMajorService.insertGwyMajorMainFeedBack(gwyMajorMain);
                }
                for (String temp : major.split("、")) {
                    List<GwyMajorMain> gwyMajorMains1 = gwyMajorService.selectGwyMajorBykey(temp, 0, typeIds[i]);
                    GwyMajorMain child;
                    if (CollectionUtils.isEmpty(gwyMajorMains1)) {
                        child = new GwyMajorMain();
                        child.setMajorName(temp);
                        child.setParentIds("," + gwyMajorMain.getId() + ",");
                        child.setStatus(CommonEnum.NOT_DELETE.getCode());
                        child.setTopStatus(0);
                        child.setTypeId(typeId);
                        child.setYear(year);
                        batchInsert.add(child);
                    } else {
                        child = gwyMajorMains1.get(0);
                        child.setParentIds(child.getParentIds() + gwyMajorMain.getId() + ",");
                        batchUpdate.add(child);
                    }
                }
                if (!CollectionUtils.isEmpty(batchInsert)) {
                    gwyMajorService.insertGwyMajorMain(batchInsert);
                }
                if (!CollectionUtils.isEmpty(batchUpdate)) {
                    gwyMajorService.updateGwyMajorMain(batchUpdate);
                }
            }
        }
        return Result.success();
    }

    @RequestMapping(value = "/public/selectType.api")
    @ResponseBody
    public Result selectType() {
        return Result.success(gwyMajorService.selectType());
    }

}
