/* */
package org.bender.easyhelp.dao.mapper;

import org.bender.easyhelp.dao.entity.GwyMajorMain;
import org.bender.easyhelp.dao.entity.GwyMajorMainCriteria;
import org.springframework.stereotype.Repository;

/**
 
 * @author CodeBender
 * date:2021/04/24 22:28
 */
@Repository
public interface GwyMajorMainDAO extends IMapper<GwyMajorMain, GwyMajorMainCriteria, Integer> {
}