/* */
package org.bender.easyhelp.dao.entity;

import java.io.Serializable;
import java.util.List;

/** 
 * @author CodeBender
 * date:2021/04/24 22:28
 */
public class GwyMajorMain implements Serializable {

    private List<GwyMajorMain> childs;

    public List<GwyMajorMain> getChilds() {
        return childs;
    }

    public void setChilds(List<GwyMajorMain> childs) {
        this.childs = childs;
    }

    /**
     * 串行版本ID
    */
    private static final long serialVersionUID = 5523131023449912209L;

    /** 
     * 专业id
     */ 
    private Integer id;

    /** 
     * 类型id
     */ 
    private Integer typeId;

    /** 
     * 专业名称
     */ 
    private String majorName;

    /** 
     * 有效年度
     */ 
    private Integer year;

    /** 
     * 是否为第一级，0不是，1是  默认：1
     */ 
    private Integer topStatus;

    /** 
     * 父亲id，多个;分割
     */ 
    private String parentIds;

    /** 
     * 0无效 1有效
     */ 
    private Integer status;

    /** 
     * 获取 专业id gwy_major_main.id
     * @return 专业id
     */
    public final Integer getId() {
        return id;
    }

    /** 
     * 设置 专业id gwy_major_main.id
     * @param id 专业id
     */
    public final void setId(Integer id) {
        this.id = id;
    }

    /** 
     * 获取 类型id gwy_major_main.type_id
     * @return 类型id
     */
    public final Integer getTypeId() {
        return typeId;
    }

    /** 
     * 设置 类型id gwy_major_main.type_id
     * @param typeId 类型id
     */
    public final void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    /** 
     * 获取 专业名称 gwy_major_main.major_name
     * @return 专业名称
     */
    public final String getMajorName() {
        return majorName;
    }

    /** 
     * 设置 专业名称 gwy_major_main.major_name
     * @param majorName 专业名称
     */
    public final void setMajorName(String majorName) {
        this.majorName = majorName == null ? null : majorName.trim();
    }

    /** 
     * 获取 有效年度 gwy_major_main.year
     * @return 有效年度
     */
    public final Integer getYear() {
        return year;
    }

    /** 
     * 设置 有效年度 gwy_major_main.year
     * @param year 有效年度
     */
    public final void setYear(Integer year) {
        this.year = year;
    }

    /** 
     * 获取 是否为第一级，0不是，1是 gwy_major_main.top_status
     * @return 是否为第一级，0不是，1是
     */
    public final Integer getTopStatus() {
        return topStatus;
    }

    /** 
     * 设置 是否为第一级，0不是，1是 gwy_major_main.top_status
     * @param topStatus 是否为第一级，0不是，1是
     */
    public final void setTopStatus(Integer topStatus) {
        this.topStatus = topStatus;
    }

    /** 
     * 获取 父亲id，多个;分割 gwy_major_main.parent_ids
     * @return 父亲id，多个;分割
     */
    public final String getParentIds() {
        return parentIds;
    }

    /** 
     * 设置 父亲id，多个;分割 gwy_major_main.parent_ids
     * @param parentIds 父亲id，多个;分割
     */
    public final void setParentIds(String parentIds) {
        this.parentIds = parentIds == null ? null : parentIds.trim();
    }

    /** 
     * 获取 0无效 1有效 gwy_major_main.status
     * @return 0无效 1有效
     */
    public final Integer getStatus() {
        return status;
    }

    /** 
     * 设置 0无效 1有效 gwy_major_main.status
     * @param status 0无效 1有效
     */
    public final void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append(", id=").append(id);
        sb.append(", typeId=").append(typeId);
        sb.append(", majorName=").append(majorName);
        sb.append(", year=").append(year);
        sb.append(", topStatus=").append(topStatus);
        sb.append(", parentIds=").append(parentIds);
        sb.append(", status=").append(status);
        sb.append("]");
        return sb.toString();
    }
}