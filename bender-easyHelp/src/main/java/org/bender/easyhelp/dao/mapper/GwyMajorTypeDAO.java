/* */
package org.bender.easyhelp.dao.mapper;

import org.bender.easyhelp.dao.entity.GwyMajorType;
import org.bender.easyhelp.dao.entity.GwyMajorTypeCriteria;
import org.springframework.stereotype.Repository;

/**
 
 * @author CodeBender
 * date:2021/04/24 22:28
 */
@Repository
public interface GwyMajorTypeDAO extends IMapper<GwyMajorType, GwyMajorTypeCriteria, Integer> {
}