/* */
package org.bender.easyhelp.dao.entity;

import java.io.Serializable;
import java.util.Date;

/** 
 * @author CodeBender
 * date:2021/04/24 22:28
 */
public class GwyMajorType implements Serializable {
    /** 
     * 串行版本ID
    */
    private static final long serialVersionUID = -8759648051257321683L;

    /** 
     * 专业类型id
     */ 
    private Integer id;

    /** 
     * 专业类型名称
     */ 
    private String typeName;

    /** 
     * 创建时间
     */ 
    private Date createTime;

    /** 
     * 0无效 1有效
     */ 
    private Integer status;

    /** 
     * 获取 专业类型id gwy_major_type.id
     * @return 专业类型id
     */
    public final Integer getId() {
        return id;
    }

    /** 
     * 设置 专业类型id gwy_major_type.id
     * @param id 专业类型id
     */
    public final void setId(Integer id) {
        this.id = id;
    }

    /** 
     * 获取 专业类型名称 gwy_major_type.type_name
     * @return 专业类型名称
     */
    public final String getTypeName() {
        return typeName;
    }

    /** 
     * 设置 专业类型名称 gwy_major_type.type_name
     * @param typeName 专业类型名称
     */
    public final void setTypeName(String typeName) {
        this.typeName = typeName == null ? null : typeName.trim();
    }

    /** 
     * 获取 创建时间 gwy_major_type.create_time
     * @return 创建时间
     */
    public final Date getCreateTime() {
        return createTime;
    }

    /** 
     * 设置 创建时间 gwy_major_type.create_time
     * @param createTime 创建时间
     */
    public final void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /** 
     * 获取 0无效 1有效 gwy_major_type.status
     * @return 0无效 1有效
     */
    public final Integer getStatus() {
        return status;
    }

    /** 
     * 设置 0无效 1有效 gwy_major_type.status
     * @param status 0无效 1有效
     */
    public final void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append(", id=").append(id);
        sb.append(", typeName=").append(typeName);
        sb.append(", createTime=").append(createTime);
        sb.append(", status=").append(status);
        sb.append("]");
        return sb.toString();
    }
}