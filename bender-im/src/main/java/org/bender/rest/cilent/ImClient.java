package org.bender.rest.cilent;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.bender.api.user.UserRemote;
import org.bender.api.user.dto.UserBindDto;
import org.bender.api.user.dto.UserInfoDto;
import org.bender.api.user.req.CommonReq;
import org.bender.common.PageParam;
import org.bender.common.Result;
import org.bender.common.ResultStatus;
import org.bender.enums.RedisKeys;
import org.bender.util.RedisUtil;
import org.jim.core.ImChannelContext;
import org.jim.core.config.ImConfig;
import org.jim.server.JimServerAPI;
import org.jim.server.config.ImServerConfig;
import org.jim.server.util.ChatKit;
import org.jim.server.util.ImgRandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.UUID;

/**
 * 用户相关的client
 *
 * @author CodeBender-zhj
 * @date 2020/8/10
 **/

@Component
public class ImClient {

    @Reference(lazy = true)
    private UserRemote tempUserRemote;
    @Autowired
    private RedisUtil tempRedisUtil;

    private static Long expireTime = 60 * 60 * 24L;

    @PostConstruct
    private void init() {
        this.userRemote = tempUserRemote;
        this.redisUtil = tempRedisUtil;
    }

    private static UserRemote userRemote;
    private static RedisUtil redisUtil;

    /**
     * 获取用户信息
     *
     * @param accountNum
     * @return
     */
    public static Result<UserInfoDto> getUserInfo(String accountNum) {
        UserInfoDto userInfoEasy = getUserInfoEasy(accountNum);
        if (null != userInfoEasy) {
            return Result.success(userInfoEasy);
        } else {
            return Result.resultStatus(ResultStatus.user_not_exist);
        }
    }

    public static UserInfoDto getUserInfoEasy(String accountNum) {
        UserInfoDto userInfoDto = (UserInfoDto) redisUtil.get(RedisKeys.IM_USER_USERID + accountNum);
        if (null != userInfoDto) {
            return userInfoDto;
        }
        Result<UserInfoDto> userDtoResult = userRemote.selectByUserToken(accountNum);
        if (null != userDtoResult && userDtoResult.getStatus() == 200 && null != userDtoResult.getData()) {
            return userDtoResult.getData();
        }
        return null;
    }

    /**
     * 添加好友
     *
     * @param req
     * @return
     */
    public static Result addFriend(CommonReq req) {
        UserInfoDto userInfoEasy = getUserInfoEasy(req.getAccountNum());
        UserInfoDto friendInfo = getUserInfoEasy(req.getToAccountNum());
        if (null == friendInfo) {
            return Result.resultStatus(ResultStatus.friend_not_exist);
        }
        req.setUserId(userInfoEasy.getId());
        req.setToUserId(friendInfo.getId());
        if (StringUtils.isEmpty(req.getNickName())) {
            req.setNickName(friendInfo.getNickName());
        }
        /**
         * 发送成功im消息
         */
        return userRemote.addFriend(req);
    }

    public static Result login(String userToken, String pwd) {
        UserBindDto build = UserBindDto.builder().userToken(userToken).build();
        Result<UserInfoDto> login = userRemote.login(build, pwd);
        if (null != login && login.getStatus() == 200 && null != login.getData()) {
            if (null == login.getData().getHeadImgFileId()) {
                login.getData().setHeadImgFileUrl(ImgRandomUtil.nextImg());
            } else {
                // todo file项目
            }
            String sessionKey = login.getData().getSessionKey();
            login.getData().setSessionKey(sessionKey);
            redisUtil.set(sessionKey, login.getData(), expireTime);
        }
        return login;
    }

    /**
     * 判断是否在线
     *
     * @param sessionKey
     * @return
     */
    public static Boolean checkOnline(String sessionKey) {
        ImServerConfig imServerConfig = ImConfig.Global.get();
        boolean isStore = ImServerConfig.ON.equals(imServerConfig.getIsStore());
        return ChatKit.isOnline(sessionKey, isStore);
    }

    /**
     *  更具accountToken 和昵称 手机号 模糊查询用户
     * @param pageNum
     * @param comId
     * @return
     */
    public static Result<PageInfo<UserInfoDto>> getUserInfoByKeyPage(PageParam pageNum, Long comId) {
        if (null == pageNum) {
            PageParam.builder().pageNum(1).pageSize(100000);
        }
        return userRemote.getUserInfoByKeyPage(pageNum, null);
    }


}
