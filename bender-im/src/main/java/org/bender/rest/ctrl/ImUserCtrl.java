package org.bender.rest.ctrl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.bender.api.user.UserRemote;
import org.bender.api.user.dto.UserInfoDto;
import org.bender.api.user.req.CommonReq;
import org.bender.common.PageParam;
import org.bender.common.Result;
import org.bender.common.ResultStatus;
import org.bender.rest.cilent.ImClient;
import org.bender.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author CodeBender-zhj
 * @date 2020/7/28
 **/
@Controller
@RequestMapping(value = "/API/imUser")
@Slf4j
public class ImUserCtrl {

	@Reference(lazy = true, timeout = 10000)
	private UserRemote userRemote;

	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private ImClient imClient;

	@RequestMapping(value = "/public/registerUser.im")
	@ResponseBody
	public Result registerUser(@Validated @RequestBody UserInfoDto userInfoDto) {
		if (null != userInfoDto) {
			return userRemote.registerUser(userInfoDto);
		}
		return Result.resultStatus(ResultStatus.error_invalid_argument);
	}

	@RequestMapping(value = "/public/login.im")
	@ResponseBody
	public Result login(@Validated @RequestParam(value = "userToken", required = true) String userToken,
			@RequestParam(value = "pwd", required = true) String pwd) {
		return imClient.login(userToken, pwd);
	}

	@RequestMapping(value = "/addFriend.im")
	@ResponseBody
	public Result addFriend(@RequestBody CommonReq req) {
		return imClient.addFriend(req);
	}

	@RequestMapping(value = "/userInfo.im")
	@ResponseBody
	public Result userInfo(@RequestBody CommonReq req) {
		return imClient.getUserInfo(req.getUuid());
	}

	@RequestMapping(value = "/getUserInfoByKeyPage.im")
	@ResponseBody
	public Result<PageInfo<UserInfoDto>> getUserInfoByKeyPage(@RequestBody PageParam pageNum, Long comId) {
		return imClient.getUserInfoByKeyPage( pageNum,  comId);
	}

}
