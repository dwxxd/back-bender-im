package org.bender.config;

import lombok.extern.slf4j.Slf4j;
import org.bender.common.Result;
import org.bender.common.ResultStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author CodeBender-zhj
 * @date 2021/2/25
 **/
@ControllerAdvice
@Slf4j
public class GlobalHandler {
	/**
	 * 所有的校验失败情况都会由这里处理
	 */
	@ResponseBody
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Result exceptionHandler(MethodArgumentNotValidException e) {
		return Result.resultStatus(ResultStatus.error_invalid_argument, e.getMessage());
	}
}
