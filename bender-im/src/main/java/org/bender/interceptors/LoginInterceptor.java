package org.bender.interceptors;

/**
 * @author CodeBender-zhj
 * @date 2020/8/11
 **/

import org.apache.commons.lang.StringUtils;
import org.bender.exceptions.LoginException;
import org.bender.util.LoginUserUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LoginInterceptor implements HandlerInterceptor {
//    @Value("${config.jwtKey}")
    private String jwt="jwt";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String jwt = request.getHeader(this.jwt);
        if (StringUtils.isEmpty(jwt)) {
            throw new LoginException();
        }
        LoginUserUtil.sessionKey = jwt;
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
