package org.bender;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.bender.util.BeanUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@ComponentScan(basePackages = { "org.bender" })
@EnableDubbo
public class BenderImRun {

	public static void main(String[] args) throws Exception {
		ConfigurableApplicationContext run = SpringApplication.run(BenderImRun.class, args);
		// 将run方法的返回值赋值给工具类中的静态变量
		BeanUtils.applicationContext = run;
	}

}
