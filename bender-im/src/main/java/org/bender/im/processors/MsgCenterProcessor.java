package org.bender.im.processors;

import com.alibaba.fastjson.JSON;
import org.jim.server.processor.chat.BaseAsyncChatMessageProcessor;
import lombok.extern.slf4j.Slf4j;
import org.jim.core.ImChannelContext;
import org.jim.core.packets.ChatBody;

/**
 * @author CodeBender
 * @date 2020/7/13 世上无难事只怕有心人
 **/
@Slf4j
public class MsgCenterProcessor extends BaseAsyncChatMessageProcessor {

	@Override
	protected void doProcess(ChatBody chatBody, ImChannelContext imChannelContext) {
		chatBody.setContent(chatBody.getContent());
		log.info("进入了信息处理中心-chatBody" + JSON.toJSONString(chatBody));
		log.info("进入了信息处理中心-ImChannelContext" + JSON.toJSONString(imChannelContext));
	}
}
