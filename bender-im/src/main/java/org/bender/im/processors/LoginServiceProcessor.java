/**
 *
 */
package org.bender.im.processors;

import org.apache.commons.lang.StringUtils;
import org.bender.api.user.dto.UserInfoDto;
import org.bender.common.Result;
import org.bender.enums.ImCommonKeys;
import org.bender.rest.cilent.ImClient;
import org.bender.util.BeanUtils;
import org.bender.util.RedisUtil;
import org.jim.server.processor.login.LoginCmdProcessor;
import org.jim.server.protocol.AbstractProtocolCmdProcessor;
import org.jim.core.ImChannelContext;
import org.jim.core.packets.*;
import org.jim.server.util.ImgRandomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @author WChao
 *
 */

public class LoginServiceProcessor extends AbstractProtocolCmdProcessor implements LoginCmdProcessor {
	private Logger logger = LoggerFactory.getLogger(LoginServiceProcessor.class);
	public static final Map<String, User> tokenMap = new HashMap<>();

	/**
	 * 根据用户名和密码获取用户
	 * 
	 * @param loginReqBody
	 * @param imChannelContext
	 * @return
	 * @author: WChao
	 */
	public User getUser(LoginReqBody loginReqBody, ImChannelContext imChannelContext) {
		String token = loginReqBody.getToken();
		User user = getUser(token);
//        user.setUserId(loginReqBody.getUserId());
		if (null != user && StringUtils.isEmpty(user.getAvatar())) {
			user.setAvatar(ImgRandomUtil.nextImg());
		}
		return user;
	}

	/**
	 * 根据token获取用户信息
	 * 
	 * @param token
	 * @return
	 * @author: WChao
	 */
	public User getUser(String token) {
		RedisUtil redisUtil = BeanUtils.getBean(RedisUtil.class);
		UserInfoDto login = (UserInfoDto) redisUtil.get(token);
		User user = null;
		if (Objects.nonNull(login)) {
			User.Builder builder = User.newBuilder()
					.avatar(null != login.getHeadImgFileUrl() ? login.getHeadImgFileUrl() + "" : "")
					.nick(login.getNickName()).userId(token).status(UserStatusType.ONLINE.getStatus());
			/**
			 * 初始化群组
			 */
			initGroups(builder);

			/**
			 * 初始化好友
			 */

			initFriends(builder, token);
			user = builder.build();
			tokenMap.put(token, user);
			return user;

		}
		return null;
	}

	/**
	 * 初始化好友
	 * 
	 * @param builder
	 */
	public void initFriends(User.Builder builder, String token) {
		Group myFriend = Group.newBuilder().groupId(ImCommonKeys.IM_USER_FRIEND + token).name("我的好友").build();
		List<User> myFriendGroupUsers = new ArrayList();
		/**
		 * 获取好友
		 */
		myFriendGroupUsers.add(builder.build());// 把自己放进去
		myFriend.setUsers(myFriendGroupUsers);
		builder.addFriend(myFriend);
	}

	/**
	 * 初始化群组
	 * 
	 * @param builder
	 */
	public void initGroups(User.Builder builder) {
		builder.addGroup(Group.newBuilder().groupId("100").name("J-IM朋友圈").build());
	}

	public String nextImg() {
		return "http://images.rednet.cn/articleimage/2013/01/23/1403536948.jpg";
	}

	/**
	 * 登陆成功返回状态码:ImStatus.C10007 登录失败返回状态码:ImStatus.C10008
	 * 注意：只要返回非成功状态码(ImStatus.C10007),其他状态码均为失败,此时用户可以自定义返回状态码，定义返回前端失败信息
	 */
	@Override
	public LoginRespBody doLogin(LoginReqBody loginReqBody, ImChannelContext imChannelContext) {
		if (Objects.nonNull(loginReqBody.getToken())) {
			RedisUtil redisUtil = BeanUtils.getBean(RedisUtil.class);
			Object o = redisUtil.get(loginReqBody.getToken());
			if (null != o) {
				return LoginRespBody.success();
			} else {
				return LoginRespBody.failed();
			}
		} else if (Objects.nonNull(loginReqBody.getUserId()) && Objects.nonNull(loginReqBody.getPassword())) {
			ImClient imClient = BeanUtils.getBean(ImClient.class);
			Result<UserInfoDto> login = imClient.login(loginReqBody.getUserId(), loginReqBody.getPassword());
			if (null != login && login.getStatus() == 200 && null != login.getData()) {
				loginReqBody.setToken(login.getData().getSessionKey());
				return LoginRespBody.success();
			}
		}
		return LoginRespBody.failed();

	}

	@Override
	public void onSuccess(User user, ImChannelContext channelContext) {
		logger.info("登录成功回调方法");
	}

	@Override
	public void onFailed(ImChannelContext imChannelContext) {
		logger.info("登录失败回调方法");
	}
}
