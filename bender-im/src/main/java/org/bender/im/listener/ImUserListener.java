package org.bender.im.listener;

import com.alibaba.fastjson.JSONObject;
import org.jim.server.listener.AbstractImUserListener;
import lombok.extern.slf4j.Slf4j;
import org.jim.core.ImChannelContext;
import org.jim.core.exception.ImException;
import org.jim.core.packets.User;

/**
 * @author CodeBender
 * @date 2020/07/13 宁波不仅没有爱情 ，还买不起房
 **/
@Slf4j
public class ImUserListener extends AbstractImUserListener {
	@Override
	public void doAfterBind(ImChannelContext imChannelContext, User user) throws ImException {
		log.info("绑定用户:{}", JSONObject.toJSONString(user));
	}

	@Override
	public void doAfterUnbind(ImChannelContext imChannelContext, User user) throws ImException {
		log.info("解绑用户:{}", JSONObject.toJSONString(user));
	}
}
