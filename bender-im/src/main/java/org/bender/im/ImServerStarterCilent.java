package org.bender.im;

import org.bender.im.processors.MsgCenterProcessor;
import org.jim.server.JimServer;
import org.jim.server.command.CommandManager;
import org.jim.server.command.handler.ChatReqHandler;
import org.jim.server.command.handler.HandshakeReqHandler;
import org.jim.server.command.handler.LoginReqHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.bender.im.listener.ImGroupListener;
import org.bender.im.listener.ImUserListener;
import org.bender.im.processors.BenderWsHandshakeProcessor;
import org.bender.im.processors.LoginServiceProcessor;
import org.jim.core.packets.Command;
import org.jim.core.utils.PropUtil;
import org.jim.server.config.ImServerConfig;
import org.jim.server.config.PropertyImServerConfigBuilder;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.tio.core.ssl.SslConfig;

/**
 * @author CodeBender
 * @date 2020/7/16
 **/
@Component
@Order(value = 1)
@Slf4j
public class ImServerStarterCilent implements CommandLineRunner {
	@Override
	public void run(String... args) throws Exception {
		new Thread(() -> {
			try {
				iniImService();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();

	}

	private static void iniImService() throws Exception {
		log.info("准备执行im服务--------------");
		ImServerConfig imServerConfig = new PropertyImServerConfigBuilder("jim.properties").build();
		// 初始化SSL;(开启SSL之前,你要保证你有SSL证书哦...)
		initSsl(imServerConfig);
		// 设置群组监听器，非必须，根据需要自己选择性实现;
		imServerConfig.setImGroupListener(new ImGroupListener());
		// 设置绑定用户监听器，非必须，根据需要自己选择性实现;
		imServerConfig.setImUserListener(new ImUserListener());
		JimServer jimServer = new JimServer(imServerConfig);
		/*****************
		 * start 以下处理器根据业务需要自行添加与扩展，每个Command都可以添加扩展,此处为demo中处理
		 **********************************/

		HandshakeReqHandler handshakeReqHandler = CommandManager.getCommand(Command.COMMAND_HANDSHAKE_REQ,
				HandshakeReqHandler.class);
		// 添加自定义握手处理器;
		handshakeReqHandler.addMultiProtocolProcessor(new BenderWsHandshakeProcessor());
		LoginReqHandler loginReqHandler = CommandManager.getCommand(Command.COMMAND_LOGIN_REQ, LoginReqHandler.class);
		// 添加登录业务处理器;
		loginReqHandler.setSingleProcessor(new LoginServiceProcessor());
		// 添加用户业务聊天记录处理器，用户自己继承抽象类BaseAsyncChatMessageProcessor即可，以下为内置默认的处理器！
		ChatReqHandler chatReqHandler = CommandManager.getCommand(Command.COMMAND_CHAT_REQ, ChatReqHandler.class);
		chatReqHandler.setSingleProcessor(new MsgCenterProcessor());
		/*****************
		 * end
		 *******************************************************************************************/
		jimServer.start();
	}

	private static void initSsl(ImServerConfig imServerConfig) throws Exception {
		// 开启SSL
		if (ImServerConfig.ON.equals(imServerConfig.getIsSSL())) {
			String keyStorePath = PropUtil.get("jim.key.store.path");
			String keyStoreFile = keyStorePath;
			String trustStoreFile = keyStorePath;
			String keyStorePwd = PropUtil.get("jim.key.store.pwd");
			if (StringUtils.isNotBlank(keyStoreFile) && StringUtils.isNotBlank(trustStoreFile)) {
				SslConfig sslConfig = SslConfig.forServer(keyStoreFile, trustStoreFile, keyStorePwd);
				imServerConfig.setSslConfig(sslConfig);
			}
		}
	}

}
