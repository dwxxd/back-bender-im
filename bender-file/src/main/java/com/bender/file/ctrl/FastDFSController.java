package com.bender.file.ctrl;

import com.bender.file.ctrl.client.FastDFSClientWrapper;
import org.bender.api.file.dto.FileDto;
import org.bender.common.Result;
import com.bender.file.service.FastFDSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author CodeBender
 * @date 2020/7/16
 * 世上无难事只怕有心人
 **/
@RestController
@RequestMapping("/fdfs")
public class FastDFSController {

    @Autowired
    private FastDFSClientWrapper fastDFSClientWrapper;

    @Autowired
    private FastFDSService fastFDSService;

    @Value("${fdfs.web-server-url}")
    private String truePath;
    @Value("${fdfs.web-port}")
    private String truePort;

    /**
     * 访问上传页面
     *
     * @return
     */
    @GetMapping("/")
    public String index() {
        return "upload";
    }

    /**
     * 返回信息
     *
     * @return
     */
    @GetMapping("/uploadStatus")
    public String uploadStatus() {
        return "uploadStatus";
    }

    /**
     * 文件上传
     *
     * @param files
     * @return
     * @throws IOException
     */
    @PostMapping("/upload")
    @ResponseBody
    public Result uploadFile(@RequestParam("file") MultipartFile[] files, @RequestParam(value = "timeSize", required = false) Long[] timeSize) throws IOException {
        if (null == files || files.length == 0) {
            return Result.fail();
        }
        List<FileDto> fileBack =new ArrayList<>();
        for (int i =0;i<files.length;i++) {
            MultipartFile file =files[i];
            byte[] bytes = file.getBytes();
            String originalFileName = file.getOriginalFilename();
            String extension = originalFileName.substring(originalFileName.lastIndexOf(".") + 1);
            String fileName = file.getOriginalFilename();
            long fileSize = file.getSize();
            String path = fastDFSClientWrapper.uploadFile(bytes, fileSize, extension);
            FileDto fileDto = new FileDto();
            fileDto.setCreater(-1L);
            fileDto.setTruePath(truePath);
            fileDto.setTruePort(truePort);
            if(null!=timeSize&&timeSize.length>0){
                fileDto.setTimeSize(timeSize[i]);
            }
            fileDto.setCreateTime(System.currentTimeMillis());
            fileDto.setFileName(fileName);
            fileDto.setUrl(path);
            fileDto.setSize(fileSize);
            fileDto.setUuid(fastFDSService.insertOrUpdateFile(fileDto));
            fileBack.add(fileDto);
        }
        return Result.success(fileBack);
    }

    /**
     * 文件下载
     *
     * @param fileUrl
     * @param fileName
     * @param response
     * @throws IOException
     */
    @RequestMapping("/download")
    public void downloadFile(String fileUrl, String fileName, HttpServletResponse response) throws IOException {
        byte[] bytes = fastDFSClientWrapper.downloadFile(fileUrl);
        // 这里只是为了整合fastdfs，所以写死了文件格式。需要在上传的时候保存文件名。下载的时候使用对应的格式
        response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        response.setCharacterEncoding("UTF-8");
        ServletOutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();
            outputStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.flush();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
