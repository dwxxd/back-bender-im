package com.bender.file.service.impl;

import org.bender.api.file.dto.FileDto;
import com.bender.file.dao.entity.File;
import com.bender.file.dao.entity.FileExample;
import com.bender.file.dao.mapper.FileMapper;
import org.bender.util.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import com.bender.file.service.FastFDSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author CodeBender
 * @date 2020/7/16
 * 世上无难事只怕有心人
 **/
@Service
@Component
public class FastFDSServiceImpl implements FastFDSService {

    @Autowired
    private FileMapper fileMapper;

    @Override
    @Transactional
    public String insertOrUpdateFile(FileDto fileDto) {
        File file = new File();
        try {
            fileDto.setUuid(UUID.randomUUID().toString());
            BeanUtils.copyProperties(file, fileDto);
            fileMapper.insertSelective(file);
            fileDto.setFileId(file.getFileId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getUuid();
    }

    @Override
    public List<FileDto> selectFilesByFileId(List<Long> fileIds) {
        List<FileDto> result = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(fileIds)) {
            FileExample example = new FileExample();
            example.createCriteria().andFileIdIn(fileIds);
            List<File> files = fileMapper.selectByExample(example);
            if (CollectionUtils.isNotEmpty(fileIds)) {
                try {
                    result = BeanUtils.copyListProperties(FileDto.class, files);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    @Override
    public Boolean batchDeleteFileById(List<Long> fileIds) {
        if (CollectionUtils.isNotEmpty(fileIds)) {
            for (Long id : fileIds) {
                fileMapper.deleteByPrimaryKey(id);
            }
        }
        return true;
    }
}
