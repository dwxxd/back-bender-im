package com.bender.file.service;

import org.bender.api.file.dto.FileDto;

import java.util.List;

/**
 * @author CodeBender
 * @date 2020/7/16
 * 世上无难事只怕有心人
 **/
public interface FastFDSService {

    /**
     *  增加或更新文件
     * @param fileDto
     * @return
     */
    String insertOrUpdateFile ( FileDto fileDto);

    /**
     * 根据fileId 获取file
     * @param fileIds
     * @return
     */
    List<FileDto> selectFilesByFileId(List<Long> fileIds);

    /**
     * 批量删除
     * @param fileIds
     * @return
     */
    Boolean batchDeleteFileById(List<Long> fileIds);

}
